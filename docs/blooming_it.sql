-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 23, 2020 at 10:11 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blooming_it`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE `assignments` (
  `id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `github_link` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `assignments`
--

INSERT INTO `assignments` (`id`, `chapter_id`, `name`, `description`, `github_link`) VALUES
(6, 77, 'Toaster Engineering Assignment', 'asdasd', 'https://github.com/caiogondim/blooming-menu.js'),
(7, 78, 'Practice', 'Practice what you learned in the video.', 'https://github.com/microsoft/verona'),
(8, 80, 'Crunching data', 'asdsad', '-'),
(9, 81, 'Chrunch more data', 'Crunch this data again: OAKDOASKDOAKDSONAQF(QFK PKSDNPSNDNSQPIDNQISNDPQNDPQSKD:Q:KNDPIQNWPDQWN DPKQNWDPINQWPDIQS:KDNQPINDPQWKD PKQWINDPQWNDPQWINDPIQWND', 'undefined'),
(10, 82, 'Transcend', '...', 'undefined');

-- --------------------------------------------------------

--
-- Table structure for table `bookmarks`
--

CREATE TABLE `bookmarks` (
  `course_id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookmarks`
--

INSERT INTO `bookmarks` (`course_id`, `user_id`) VALUES
(43, 47715382),
(45, 47715382);

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE `chapters` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chapters`
--

INSERT INTO `chapters` (`id`, `name`, `subject_id`) VALUES
(76, 'Chapter 1', 43),
(77, 'Chapter 2', 43),
(78, 'Chapter 1', 44),
(79, 'Chapter 1', 45),
(80, 'Phase 1', 46),
(81, 'Phase 2', 46),
(82, 'Phase 3', 46);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `parent_comment_id` int(11) NOT NULL,
  `user_assignment_id` int(11) NOT NULL,
  `rating` decimal(10,0) NOT NULL,
  `content` text NOT NULL,
  `date_added` date NOT NULL,
  `question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `parent_comment_id`, `user_assignment_id`, `rating`, `content`, `date_added`, `question_id`) VALUES
(12, 47715382, -1, -1, '0', '<p>So I tried some things and it ended up working, thanks for the help still.</p>', '2020-02-29', 13),
(13, 47715382, 12, 0, '0', 'Sorry for the inconvenience!', '2020-01-23', 13);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `name` varchar(155) NOT NULL,
  `description` varchar(3255) NOT NULL,
  `location` varchar(155) NOT NULL,
  `category_id` int(11) NOT NULL,
  `time` varchar(5) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `description`, `location`, `category_id`, `time`, `date`) VALUES
(2, 'Drinks!', 'Come and drink for charity', 'somestreet', 1, '18:00', '2020-01-01'),
(6, 'Meet up with people', 'This is the place to make new friends!', 'Somestraat 123, 4567AB Deventer', 1, '17:00', '2019-12-15'),
(8, 'Picnic', 'Come for drinks and some food with nice people.', 'Saxion, Deventer', 1, '13:30', '2020-01-29'),
(9, 'Hut Building Competition', 'May the best house win.', 'Saxion, Deventer', 1, '15:00', '2020-01-22');

-- --------------------------------------------------------

--
-- Table structure for table `event_categories`
--

CREATE TABLE `event_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event_categories`
--

INSERT INTO `event_categories` (`id`, `name`) VALUES
(1, 'Meetup'),
(2, 'Charity'),
(3, 'Workshop'),
(4, 'Party'),
(10, 'Outdoors');

-- --------------------------------------------------------

--
-- Table structure for table `event_files`
--

CREATE TABLE `event_files` (
  `event_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event_files`
--

INSERT INTO `event_files` (`event_id`, `file_id`) VALUES
(2, 2),
(6, 3),
(8, 73),
(9, 74),
(9, 75);

-- --------------------------------------------------------

--
-- Table structure for table `event_users`
--

CREATE TABLE `event_users` (
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event_users`
--

INSERT INTO `event_users` (`user_id`, `event_id`) VALUES
(47715382, 7);

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `subject_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `subject_id`) VALUES
(2, 'Is Python that good for machine learning?', 'Python is one of the best languages for that, yes!', 43),
(3, 'What is the level of this course?', 'Intermediate.', 43),
(4, 'Is there a lot of math in algorithms?', 'Yep.', 44);

-- --------------------------------------------------------

--
-- Table structure for table `feedback_requests`
--

CREATE TABLE `feedback_requests` (
  `id` int(11) NOT NULL,
  `assignemnt_id` int(11) NOT NULL,
  `giver_id` int(11) NOT NULL,
  `accepted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `name`, `type`) VALUES
(1, 'thumbnail.png', 'image'),
(2, 'https://images.theconversation.com/files/307002/original/file-20191215-123983-6wr9fd.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=1200&h=1200.0&fit=crop', 'url'),
(3, 'https://addicted2success.com/wp-content/uploads/2016/12/Changing-the-Way-You-Interact-Can-Change-Your-Life.jpg', 'url'),
(4, 'https://i.ytimg.com/vi/mypHlC64on4/maxresdefault.jpg', 'url'),
(68, 'http://www.supperstegelzetbedrijf.nl/images/supperstegelzetbedrijfnl/114-home-bultino-webshop-4870.jpg', 'url'),
(69, 'http://www.supperstegelzetbedrijf.nl/images/supperstegelzetbedrijfnl/114-home-bultino-webshop-4870.jpg', 'url4'),
(70, 'http://www.supperstegelzetbedrijf.nl/images/supperstegelzetbedrijfnl/114-home-bultino-webshop-4870.jpg', 'url2'),
(71, 'https://image.shutterstock.com/image-vector/vector-illustration-people-having-picnic-260nw-789204034.jpg', 'url'),
(72, 'https://image.freepik.com/vrije-vector/picnic-tijd-achtergrond_23-2147555449.jpg', 'url'),
(73, 'https://www.fabhotels.com/blog/wp-content/uploads/2019/05/Picnic-Spots-in-Chandigarh_600.jpg', 'url'),
(74, 'https://i0.wp.com/tinyhouseprep.com/wp-content/uploads/2018/11/primitive-technology-1024x577.jpg?resize=640%2C361', 'url'),
(75, 'https://i.pinimg.com/originals/65/48/86/65488640b84ee02e4d8a2f001d4b083e.jpg', 'url');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`) VALUES
(3, 'Data Science'),
(4, 'Algorithms');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `name`) VALUES
(1, 'admin'),
(2, 'teacher'),
(4, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `group_id` int(11) NOT NULL,
  `overview` varchar(1000) NOT NULL,
  `lecturer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `group_id`, `overview`, `lecturer_id`) VALUES
(43, 'Python Machine Learning', 3, 'This course will be covering the basics of machine learning with Python.', 47715382),
(44, 'C++ Master Class', 4, 'This master class will cover all algorithms which C++ is capable of handling.', 47715382),
(45, 'Standford Seminar - Machine Learning', 3, 'The Standford Professor Ng will walk you through the basics of machine learning.', 47715382),
(46, 'Crunching data', 3, 'Only for data crunchers.', 47715382),
(47, 'The Purpose of Data', 3, 'Explaining the purpose of data.', 47715382),
(48, 'More Data', 3, 'For those who\'ve done all else.', 47715382),
(49, 'Complex Geometry Deep Dive', 4, 'We\'ll dive deeper into the most complex parts of geometry.', 47715382),
(50, 'The Purpose of Data', 3, 'Explaining the purpose of data.', 47715382),
(51, 'Simple Algorithms', 4, 'For kindergardeners.', 47715382),
(52, 'More Data', 3, 'For those who\'ve done all else.', 47715382),
(53, 'Yet Another Algorithms Course', 4, 'Teaching you all that you\'ve seen but \"better\".', 47715382),
(54, 'Complex Geometry Deep Dive', 4, 'We\'ll dive deeper into the most complex parts of geometry.', 47715382);

-- --------------------------------------------------------

--
-- Table structure for table `threads`
--

CREATE TABLE `threads` (
  `id` int(11) NOT NULL,
  `assignment` int(11) NOT NULL,
  `chapter` int(11) NOT NULL,
  `content` text NOT NULL,
  `date_asked` date NOT NULL,
  `points` int(11) NOT NULL,
  `subject` text NOT NULL,
  `title` text NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `threads`
--

INSERT INTO `threads` (`id`, `assignment`, `chapter`, `content`, `date_asked`, `points`, `subject`, `title`, `user_id`) VALUES
(13, 0, 0, 'Tried to set it up but couldn\'t do it.', '2020-01-23', 0, 'Python Machine Learning', 'Need help with Python', 47715382);

-- --------------------------------------------------------

--
-- Table structure for table `tutorials`
--

CREATE TABLE `tutorials` (
  `id` int(11) NOT NULL,
  `assignemnt_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `first_name`, `last_name`, `role_id`) VALUES
(47715382, 'kailoan', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_assignements`
--

CREATE TABLE `user_assignements` (
  `id` int(11) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` double NOT NULL,
  `content` text NOT NULL,
  `repository` varchar(255) NOT NULL,
  `done` tinyint(1) NOT NULL,
  `date_finished` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_vote`
--

CREATE TABLE `user_vote` (
  `id` int(11) NOT NULL,
  `comment` int(11) DEFAULT NULL,
  `user_assignment` int(11) DEFAULT NULL,
  `question` int(11) DEFAULT NULL,
  `user` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT 'True for an upvote and False for a downvote'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_vote`
--

INSERT INTO `user_vote` (`id`, `comment`, `user_assignment`, `question`, `user`, `type`) VALUES
(6, NULL, NULL, 13, 47715382, 1);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `chapter_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `title`, `link`, `chapter_id`) VALUES
(9, 'Intro Video', 'https://www.youtube.com/embed/OGxgnH8y2NM?enablejsapi=1&origin=https%3A%2F%2Fmdbootstrap.com', 76),
(10, 'Regression Intro Video', 'https://www.youtube.com/embed/JcI5Vnw0b2c?enablejsapi=1&origin=https%3A%2F%2Fmdbootstrap.com', 76),
(11, 'Introduction to Algorithms', 'https://www.youtube.com/embed/J908ciES2XQ?enablejsapi=1&origin=https%3A%2F%2Fmdbootstrap.com', 78),
(12, 'Lecture 1', 'https://www.youtube.com/embed/UzxYlbK2c7E?enablejsapi=1&origin=https%3A%2F%2Fmdbootstrap.com', 79),
(13, 'Lecture 2', 'https://www.youtube.com/embed/5u4G23_OohI?enablejsapi=1&origin=https%3A%2F%2Fmdbootstrap.com', 79),
(15, 'Lecture 3', 'https://www.youtube.com/embed/HZ4cvaztQEs?enablejsapi=1&origin=https%3A%2F%2Fmdbootstrap.com', 79);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assignments`
--
ALTER TABLE `assignments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chapter_id` (`chapter_id`);

--
-- Indexes for table `chapters`
--
ALTER TABLE `chapters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subject_id` (`subject_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `parent_comment_id` (`parent_comment_id`),
  ADD KEY `user_assignment_id` (`user_assignment_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `event_categories`
--
ALTER TABLE `event_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_files`
--
ALTER TABLE `event_files`
  ADD PRIMARY KEY (`event_id`,`file_id`),
  ADD KEY `file_id` (`file_id`);

--
-- Indexes for table `event_users`
--
ALTER TABLE `event_users`
  ADD PRIMARY KEY (`user_id`,`event_id`),
  ADD KEY `event_id` (`event_id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_requests`
--
ALTER TABLE `feedback_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignemnt_id` (`assignemnt_id`),
  ADD KEY `giver_id` (`giver_id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `threads`
--
ALTER TABLE `threads`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tutorials`
--
ALTER TABLE `tutorials`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignemnt_id` (`assignemnt_id`),
  ADD KEY `file_id` (`file_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `role_id_2` (`role_id`);

--
-- Indexes for table `user_assignements`
--
ALTER TABLE `user_assignements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignment_id` (`assignment_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_vote`
--
ALTER TABLE `user_vote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vote_user_idx` (`user`),
  ADD KEY `vote_question_idx` (`question`),
  ADD KEY `vote_assignment_idx` (`user_assignment`),
  ADD KEY `vote_comment_idx` (`comment`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assignments`
--
ALTER TABLE `assignments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `chapters`
--
ALTER TABLE `chapters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `event_categories`
--
ALTER TABLE `event_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `feedback_requests`
--
ALTER TABLE `feedback_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `threads`
--
ALTER TABLE `threads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tutorials`
--
ALTER TABLE `tutorials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47715383;

--
-- AUTO_INCREMENT for table `user_assignements`
--
ALTER TABLE `user_assignements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_vote`
--
ALTER TABLE `user_vote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `event_files`
--
ALTER TABLE `event_files`
  ADD CONSTRAINT `event_id` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `file_id` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `user_vote`
--
ALTER TABLE `user_vote`
  ADD CONSTRAINT `vote_assignment` FOREIGN KEY (`user_assignment`) REFERENCES `user_assignements` (`id`),
  ADD CONSTRAINT `vote_comment` FOREIGN KEY (`comment`) REFERENCES `comments` (`id`),
  ADD CONSTRAINT `vote_question` FOREIGN KEY (`question`) REFERENCES `threads` (`id`),
  ADD CONSTRAINT `vote_user` FOREIGN KEY (`user`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
