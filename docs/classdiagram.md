# Class Diagram (PlantUML)
## NOTE
When you have seen the other documents you might be thinking "Hey where is the class for a form item".
Well, This is because I'm using the UserAssignment as post, Assignment as sub-threads and subjects as main threads.
Less classes and more functionality.    

## PlantUML
```plantuml
@startuml

title Class Diagram - Server and Clients

class User {
  +Int Id
  -String Username
  -String Firtname
  -String Lastname
  -Role Role
  void SetRole(Role role)
  Role GetRole()
  string GetFullname()
  string GetUsername()
  void SetUsername(String username)
  void SetFirstname(String firstname)
  void SetLastname(String lastname)
}

class FeedbackRequest {
    +Int Id
    -UserAssignment For
    -User Giver
    -Boolean Accepted
    UserAssignment GetUAssignment()
    User GetUser()
    void SetStatus()
}

class Group {
    +Int Id
    -String Name
    String GetName()
    Void SetName(String name)
}

class Subject {
    +Int Id
    -String Name
    -Group Grp
    void SetName(String name)
    String GetName()
    Group GetGroup()
    void SetGroup(Group group)
}

class Chapter {
    +Int Id
    -String Name
    -Subject Subj
    void SetName()
    String GetName()
    void SetSubject()
    Subject GetSubject()
}

class Assignment {
    +Int Id
    -Chapter Chapt
    -String Name
    void SetChapter()
    Chapter GetChapter()
    void SetName()
    String GetName()
}

class UserAssignment {
    +Int Id
    -Assignment Assignment
    -User User
    -Int Rating
    -String Content
    -String Repository
    -Boolean Done
    -Date DateFinished
    Assignment GetAssingment()
    User GetUser()
    void SetStatus(boolean isDone)
    boolean IsDone()
    void SetContent(String content)
    string GetContent()
    void SetRepository(String repository)
    String GetRepository()
    Date GetFinishedDate()
}

class Comment {
    +Int Id
    -User Commenter
    -Comment ParentComment
    -UserAssignment Thread
    -Int Rating
    -String Content
    -Date DateAdded
    void UpVote()
    void DownVote()
    int GetRating()
    string GetContent()
    void SetContent()
    Date GetDateAdded()
}

class Rating {
    +Int Id
    -User Rater
    -Int Type
    -UserAssignment UAssignment
    -Comment Cmt
    User GetRater()
    int GetType()
    UserAssignment GetUserAssignment()
    Comment GetComment()
}

class File {
    +Int Id
    -String FileName
    -String FileType
    void SetFileName()
    String GetFileName()
    void SetFileType()
    String GetFileType()
    void GetFileType()
}

class Tutorial {
    +Int Id
    -Assignment assignment
    -File Attachment
    void SetAssignment()
    Assignment GetAssignment()
    void SetAttachment()
    File GetAttachment()
}

class FAQ {
    +Int Id
    +String Question
    +String Answer
}

class Role {
    +Int Id
    -String Name
    String GetRole()
    void SetRole()
}

User --> "1" Role
UserAssignment --> "1" User
UserAssignment "1..*" <--* "1" Assignment
Rating "1" *--> "1" UserAssignment
Rating "1" *--> "1" User
Rating "1" *--> "1" Comment
Subject "1" *--> "1..*" Chapter
Subject "1" o--> "1" Group
Chapter "1" *--> "1..*" Assignment
FeedbackRequest "1" *--> "1" UserAssignment
FeedbackRequest "1" --> "1" User
Tutorial "1..*" <--* "1" Assignment
Tutorial "1" *--> "1" File
Comment *--> Comment : Regular comment
Comment "0..*" <--* "1" Assignment : Thread relation

@enduml
```