Meeting 01 BloomingIT - Saxion Students 22/11/2019

Questions:


Luuk:

1.	Are admins responsible for adding and deleting accounts for students and teachers? Or can students and teachers sign up themselves
2.	What does an admin do that teachers don’t?
3.	Who can start a community?
4.	Who can post on the bulletin board?
5.	Can everyone chat to everyone or are there restraints
6.	Are there exciting school accounts or are there separate accounts? 
7.	You’re talking about entering videos. is that teacher of student side
8.	You don’t want us to make course material, but who does?
9.	Teachers need to be linked to students, who does that?
10.	Who can see the study progress?
11.	What are restraints for a community and who enforces this?
12.	Is lesson material only available for specific people or can everyone see everything?
13.	How is the lesson material divided, is new content added regularly, and if it is a new version of an already existing course will it be overwritten? (blackboard previous years are still visible)

UI/UX:
1. What differentiates you from applications like udemy and lynda? 
2. Are the forum and events supposed to be on the up front of the app or are they just additions to the overall feel?
3. Do you prefer a colourful or a formal (black - white) color scheme?
4. How do you want the assignments to be graded - automatically, by a teacher, or just have videos explaining the solution of an assignment given at the end of the previous video or maybe something else?
5. Also if possible would be nice if the customer can take a look at the mockups, they are in a folder (only the mobile ones have been uploaded for the time being).

Michael:
1. Does the client want a native mobile application?
2. Does the client have preferences for the frame work we use for the front end development?
3. Does have the client have preferences for technologies used in the back-end?

