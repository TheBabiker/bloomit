# Plan of approach

## Project objective

This project we are going to build an application for web and tablets.
The goal of the application is that people can learn the basics of programming. They will login via github, and on the site they can find videos and assignments. 
Every assignment will also get a forum thread, where questions related to the assignment can be asked. 
When the student is done with the assignment, a teacher can rate their assignment. 
The application will also have a events page, where future events held by Blooming IT can be posted.

## Planning
First Meeting with customer: 22/11/2019.

Second Meeting with customer:  20/12/2019.

### Sprint 0:

Start sprint 0 ( week 1 ): 18/11/2019.
Reflection sprint 0 ( week 2 ): 2/12/2019.

Sprint 0 will mostly be about understanding the domain and realising some documentation and a start for the codebase.

### Sprint 1:
1. Start sprint 1 ( week 3 ): 2/12/2019.
2. Reflection sprint 1 ( week 5 ): 16/12/2019.

Sprint 1 is mostly focused around coding and making sure that the must requirements are met.  

### Sprint 2:

1. Start sprint 2 ( week 6 ): 6/1/2020.
2. Reflection sprint 2 ( week 8 ):20/1/2020.

Sprint 2 will be mostly about fixing bugs and implementing should requirements.

## Requirements
Requirement can be found in the functional design.
