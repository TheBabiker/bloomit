tit- `GET '/api/forum/questions'` — Get all questions.

  Status code 200. Response body example:

  ```js
    [{
        "id": 1,
        "content": "Some content",
        "date_asked": "2019-12-19T23:00:00.000Z",
        "username": "user",
        "title": "A title"
    }]
  ```

  Status code 404. Response body example:

  ```js
  "No matching results."
  ```

`POST /forum/:item/:id/vote` - Vote for a thread.
  item = "userassignment" or "question"
  id = id of post

    Status code 200. Response body example:

    ```js
    {
        "msg": "created/updated"
    }
    ```

`GET /forum/:item/:id/votes` - Get all votes for a question or userassignment by its ID

  item = "userassignment" or "question"
  id = id of post

    Status code 200. Response body example:

  ```js
    {
       0
    }
  ```

- `GET /forum/:item/:id/comments` - Get all comments for a thread by its id

    item = "userassignment" or "question"
    id = id of item

    Status code 200 Response body example:

    ```js
    [
        {
            "id": 8,
            "parentCommentId": -1,
            "content": "jdjdjdj",
            "dateAdded": "2020-02-28T23:00:00.000Z",
            "username": "mariyka",
            "children": [
                {
                    "id": 16,
                    "parentCommentId": 8,
                    "content": "ttt",
                    "dateAdded": "2020-01-21T23:00:00.000Z",
                    "username": "mariyka",
                    "children": [
                        {
                            "id": 17,
                            "parentCommentId": 16,
                            "content": "Hello?",
                            "dateAdded": "2020-01-21T23:00:00.000Z",
                            "username": "mariyka",
                            "children": null
                        },
                        {
                            "id": 19,
                            "parentCommentId": 16,
                            "content": "nice",
                            "dateAdded": "2020-01-21T23:00:00.000Z",
                            "username": "mariyka",
                            "children": null
                        }
                    ]
                }
            ]
        }
    ]
    ```

- `GET /form/popular` - Get most popular posts

    Status code 200 response body example.

    ```js
        [{
        "id": 1,
        "content": "Some content",
        "date_asked": "2019-12-19T23:00:00.000Z",
        "username": "user",
        "title": "A title"
        }]
    ```

- `GET /api/forum/question/:id` — Get individual question by id.

    Status code 200. Response body example:

  ```js
    {
        "id": 1,
        "content": "Some content",
        "date_asked": "2019-12-19T23:00:00.000Z",
        "username": "user",
        "title": "A title"
    }
    ```

  Status code 404. Response body example:

  ```js
  "No matching results."
  ```

- `GET /api/forum/userassignment/:id` - Get individual userassignments
    Status code 200. Response body example:

    ```js
        {
            "id": 1,
            "content": "Some content",
            "date_asked": "2019-12-19T23:00:00.000Z",
            "username": "user",
            "title": "A title"
        }
    ```

    Status code 404. Response body example:

    ```js
    "No matching results."
    ```

- `GET /api/forum/:item/:id/rating` - Get rating for a post
    item = "userassignment" or "question"
    id = id of item

    Status code 200 Response body example:

    ```js
        {
            0
        }
    ```

- `POST /forum/question/minimal/:title/:content/:subject/:user_id/:date_asked/:points` — Create new thread.

    Add a thread to the forum

    Request body example:

  ```js
    "None"
  ```

  Status code 200. Response body example:

  ```js
    {
        "id": 1,
        "content": "Some content",
        "date_asked": "2019-12-19T23:00:00.000Z",
        "username": "user",
        "title": "A title"
    }
  ```

  Status code 500. Response body example:

  ```js
  "Thrown error"
  ```


