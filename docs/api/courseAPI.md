**Course API - displayed through the assignments' CRUD**
- `GET /course/:courseId/chapters/:chapterId/assignments` — Get all assignments for a specific course and chapter.

  Response body example for courseId = 44 and chapterId = 78:

  ```js
    [
        {
            "id": 7,
            "name": "Practice",
            "chapterId": 78,
            "description": "Practice what you learned in the video.",
            "githubLink": "https://github.com/microsoft/verona"
        }
    ]
  ```

- `POST /course/chapters/assignments/:chapterId/:name` — Create new assignment for a specific chapter.

    name will always be DefaultName upon assignment creation if done through the application and not through Postman or other similar service.

    Request body example:

  ```js
  {
    "Inserted!"
  }
  ```

- `PUT /course/chapters/assignments/edit/:chapterId/:assignmentId/:name/:description` — EDIT assignment by id.

  There is also a github_link value that comes from the body

  Request body example for `/course/chapters/assignments/edit/78/7/PRACTICE/Practice what you learned in the video. If you have any questions look in the FAQ or the forum.`:

  ```js
  'Changed!'
  ```

- `DELETE /course/chapters/assignments/:chapterId/:assignmentId` — Delete assignment by id.

    Result for `/course/chapters/assignments/44/11`:

  ```js
  "Deleted assignment!"
  ```
  **Course API - displayed through the courses' CRUD**
  
  - `DELETE /course/:id/` — Delete's a course depending on its id.
    Result for `/course/1`:
    
   ```js
    "Deleted course!"
    ```
    
     - `GET /course/:id/'` — Get course based on given id.
     
     Result for `/course/44`:
      
       ```js
         [
             {
                  "id": 44,
                  "name": "C++",
                  "groupId": 2,
                  "lectureId": 3,
             }
         ]
       ```
       
   - `GET /courses'` — Get all courses
   
   Result for `/courses`: Returns all courses in database.
