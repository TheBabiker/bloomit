tit- `GET /events` — Get all events.

  Status code 200. Response body example:

  ```js
    [{
        "id": 2,
        "name": "Drinks!",
        "description": "this is a description",
        "location": "somestreet",
        "time": "18:00",
        "date": "1-1-2020",
        "pictures": [
            "https://specials-images.forbesimg.com/imageserve/1026205392/960x0.jpg"
        ],
        "category": "Charity",
        "attendees": []
    },
    {
        "id": 1,
        "name": "Meet up with people",
        "description": "this is a description",
        "location": "Somestraat 123, 4567AB Deventer",
        "time": "17:00",
        "date": "15-12-2019",
        "pictures": [
            "https://specials-images.forbesimg.com/imageserve/1026205392/960x0.jpg",
            "https://freshome.com/wp-content/uploads/2018/09/contemporary-exterior.jpg",
            "https://www.billboard.com/files/styles/768x433/public/media/ibiza-elrow-2019-billboard-1500.jpg"
        ],
        "category": "Meetup",
        "attendees": [
            "tranmani2"
        ]
    }
]
  ```

  Status code 404. Response body example:

  ```js
  "No matching results."
  ```

- `GET /events?category=meetup` — Get all filtered events in category Meetup.

  Currently there are 4 category: Meetup, Charity, Party and Workshop

    Status code 200. Response body example:

    ```js
      [{
          "id": 1,
          "name": "Meet up with people",
          "description": "this is a description",
          "location": "Somestraat 123, 4567AB Deventer",
          "time": "17:00",
          "date": "15-12-2019",
          "pictures": [
              "https://specials-images.forbesimg.com/imageserve/1026205392/960x0.jpg",
              "https://freshome.com/wp-content/uploads/2018/09/contemporary-exterior.jpg",
              "https://www.billboard.com/files/styles/768x433/public/media/ibiza-elrow-2019-billboard-1500.jpg"
          ],
          "category": "Meetup",
          "attendees": [
              "tranmani2"
          ]
      }]
    ```

    Status code 404. Response body example:

    ```js
    "No matching results."
    ```

- `GET /events/:id` — Get individual event by id.

    Status code 200. Response body example:

  ```js
      [{
          "id": 1,
          "name": "Meet up with people",
          "description": "this is a description",
          "location": "Somestraat 123, 4567AB Deventer",
          "time": "17:00",
          "date": "15-12-2019",
          "pictures": [
              "https://specials-images.forbesimg.com/imageserve/1026205392/960x0.jpg",
              "https://freshome.com/wp-content/uploads/2018/09/contemporary-exterior.jpg",
              "https://www.billboard.com/files/styles/768x433/public/media/ibiza-elrow-2019-billboard-1500.jpg"
          ],
          "category": "Meetup",
          "attendees": [
              "tranmani2"
          ]
      }]
    ```

  Status code 404. Response body example:

  ```js
  "No matching results."
  ```

- `POST /events` — Create new event.

    First picture and pictureType is mandatory

    Request body example:

  ```js
  {
    "title": "huy4",
    "description": "huy2",
    "location": "huy3",
    "category": "charity",
    "time": "17:00",
    "date": "2020-01-01",
    "numberOfPicture": 3,
    "picture": "http://www.supperstegelzetbedrijf.nl/images/supperstegelzetbedrijfnl/114-home-bultino-webshop-4870.jpg",
    "pictureType": "url",
    "picture2": "http://www.supperstegelzetbedrijf.nl/images/supperstegelzetbedrijfnl/114-home-bultino-webshop-4870.jpg",
    "pictureType2": "url",
    "picture3": "http://www.supperstegelzetbedrijf.nl/images/supperstegelzetbedrijfnl/114-home-bultino-webshop-4870.jpg",
    "pictureType3": "url"
  }
  ```

  Status code 201. Response body example:

  ```js
  "Add new event successfully"
  ```

  Status code 400. Response body example:

  ```js
  "Not all information is provided"
  ```

  Status code 400. Response body example:

  ```js
  "Time format should be HH:MM"
  ```
- `POST /events/:id` — Subscribe or Unsubscribe to an event by id.

  Request body example:

    ```js
    {
      "username": "tranmani"
    }
    ```

  Status code 201. Response body example:

  ```js
  "Subscribe to event successfully"
  ```

  Status code 200. Response body example:

  ```js
  "Unsubscribe to event successfully"
  ```

  Status code 401. Response body example:

  ```js
  "Please login to subscribe to event"
  ```

  Status code 400. Response body example:

  ```js
  "There is no event with id: :id"
  ```

  Status code 400. Response body example:

  ```js
  "There is no user with username: req.body.username"
  ```



- `PUT /events/:id` — EDIT event by id.

  For now it can only edit current picture, can't add more picture but can delete picture by empty it's information

  Request body example:

  ```js
  {
	  "title": "con cac",
    "description": "please come",
    "location": "Somestraat 123, 4567AB Deventer",
    "category": "charity",
    "time": "17:00",
    "date": "2019-12-12",
    "picture": "http://www.supperstegelzetbedrijf.nl/images/supperstegelzetbedrijfnl/114-home-bultino-webshop-4870.jpg",
    "pictureType": "url",
    "picture2": "http://www.supperstegelzetbedrijf.nl/images/supperstegelzetbedrijfnl/114-home-bultino-webshop-4870.jpg",
    "pictureType2": "url",
    "picture3": "http://www.supperstegelzetbedrijf.nl/images/supperstegelzetbedrijfnl/114-home-bultino-webshop-4870.jpg",
    "pictureType3": "url"
  }
  ```

  Status code 200. Response body example:

  ```js
  "Edit event successfully"
  ```

  Status code 400. Response body example:

  ```js
  "Not all information is provided"
  ```

  Status code 400. Response body example:

  ```js
  "Time format should be HH:MM"
  ```

  Status code 400. Response body example:

  ```js
  "There is no event with id: :id"
  ```


- `DELETE /events/:id` — Delete event by id.

  Status code 200. Response body example:

  ```js
  "Delete event successfully"
  ```
  
  Status code 400. Response body example:

  ```js
  "There is no event with id: :id"
  ```