# **Code of Conduct Clients and servers**
### Chapter 1: Team structure
This project we will work with different subteams, working on their own functional issues. This means there will be no "backend" and "frontend" teams. There are 5 subteams. All subteams have their own scrum master. In each team there is also a backend and frontend specialist. Every specialisation and the scrummasters will have their own meeting. This is to ensure good communication between all of the team members.

### Chapter 2: Communication between teams
If a team member has a question that is regarding multiple subteams, or regarding a different subteam, he or she will ask this question to the scrum master of his team. the scrum master will redistribute this question to the appropriate scrum master, who will ask this question in his subteam.

### Chapter 3: Communication methods
Communication between team members happens via WhatsApp. There are WhatsApp groups for every sub team, and also for the scrum masters group. there is also a WhatsApp group per specialisation.

### Chapter 4: Meetings
There are several meeting during the week for this project. There are the general meetings, those that are already scheduled by Saxion. When necessary, sub groups can also schedule meetings. These meetingss have to be agreed on by every team member, and when scheduled, every team member is expected to be there. 

### Chapter 5: Git usage
Every subteam has their own sub-master branch on the GitLab repository. Branches to work on smaller issues and personal branches will be created of such sub branch. merging into the sub master branch requires approval of one of the other team members. The scrum master will merge the sub master branch approximately once a week, after approval of one of the other scrum masters.

### Chapter 6: Absence
If a team member can't make it to a planned meeting, he or she has to inform the scrum master of the team he or she is in at least 15 minutes in advamce. If the team member is too late with informing, does not inform at all, or does not have valid reasoning, there will be consequences, stated later in this document. 

### Chapter 7: Coding standards
For the frontend we are using Vue.js. For the backend we are using Node.js and FireBase


### Chapter 8: Strike system

We are using a three-strike rule. This means, that if a team member does not adhere to the code of conduct, does'nt invest enough hours, or acts out of line otherwise, a strike can be given. 
The first strike does not have any consequences, but is a formal warning.
If a team member gets a second strike, his or her grade will be lower than the rest of the group. 
A strike can be given by the teacher. Group members can give each other strikes. However, the group should be unamimous. Strikes given by team members have to be enforced by the teacher.
When the third and last strike is given, the team member will be excluded from the project.