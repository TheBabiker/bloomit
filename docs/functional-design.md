# Functional Design

## Background

    BloomingIt is an It Company located in Enschede that develops software products to make IT more accesible for different target groups.
    Examples the client gave were connecting students and companies and helping people to learn to code.
    During the presentation at the first meeting, the client expressed that they also wanted to reach people that attended their workshops and wanted to continue learning, and teach childern how to code.

### Clients name

    BloomingIT

### Goal

    The Client wants a prototype of an application.
    That helps people take the first steps in software-engineering.

### Requirements

#### Must

     1 The Application must be available in the web browser and tablet
     2 Users must be able to sign-up with their Github account
     3 The Application must have a forum for the users (users create topics, users create comments under those topics)
     4 The application must have a bulletin board for announcements
     5 A Teacher must be able to create new courses and populate them with videos and assignments
     6 Admin must be able to view all users
     7 Admin must be able to assign roles to users
     8 Admin must be able to create new events on the bulletin board
     9 Users must be able to view course videos
     10 The application must support at least Dutch and English languages

#### Should

     1 Users should be able to submit assignments by providing Github repository links or pull   request
     2  User should have a profile page in the application
     3 The teacher should be able to edit uploaded (assignments/courses)
     4 Users should be able to keep track of courses they are following
     5 Users should be able to filter and search through the application’s contents (courses, forum, events)
     6 Users should be able to bookmark events 
     7 Users should be able to join forum categories
     8 Bookmarked events should be displayed in the user’s profile page
     9 Saved courses should be displayed in the user’s profile page
     10 Recommended courses should be displayed in the user’s profile page

#### Could

     1 Users could be able to change the layout of their profile page
     2 User could be able to hide specific sections of their profile page
     3 Users could have a profile picture which matched thri github one
     4 The forum could have different categories created by the admin
     5 Forum topics could be assigned to categories
     6 Users could be able to join forum categories     
     7 Users could Upvote/Downvote topics and comments in topics
     8 Users could be able to report content as inappropriate
     9 Admins could flag content and get notified of reported content
     10 Flagged content shouldn’t be visible to normal users (students/teachers)
     11 Users could be able to join/leave forum categories 
     12 Joined forum categories events could be displayed in the user’s profile page


#### Would

     1 The Application would be available on mobile devices
     2 The Application Would have a intergrated assignment UI

## Use-case Diagrams

![alt usecase diagram](usecase-diagram.jpg)

### Admin

     The Admin accounts will have full access to the system. They can add, edit and remove material from the application.
     An Admin has the right to give one on one (1:1) feedback. Only admins can give a user the ‘Teacher’ role if an
     user has earned enough credits from other users. See “user’;
     The Admin can block or remove users.
     The admins can edit the FAQ section.

### User

     A user can get access to the webpage via a github account. When logged in it can see video and make assignments.
     If a user has a question it can use the Peer to Peer feedback. All users, no matter what the role is, can ask and answer questions from other users.
    All users can give credits to another user when the answer is helpful.  
     If a user want 1:1 feedback it can ask this via a message to accounts with the Admin or Teacher role.
     If a User is active in the peer to peer feedback section and has a good understanding of the basics of programming
     it can get enough credits to step up a level. The Admins can change your role from “user” to “teacher”.

### Teacher

     As teacher you will have all the same functions as the “User” role. However you will get some additional features.
     Users with this role can answer in 1:1 feedback. In the peer to peer section this user will get the clear text “Teacher” added with his account name.
     Other users can see that the answer a teacher is giving is usually helpful.
     It gives the people with a ‘user’ role a fast and clear overview if a teacher gave answer or If the answer is from a regular user.

## Context Diagram

![alt context diagram](context-diagram.png)

## Data Flow Diagram

![alt dataflow diagram](dataflow-diagram.png)

### Functionality of the new IT infrastructure

    The main goal of our new application is to get people interested in programming, and to help those who are interested along their way of becoming a propper programmer. We will try to achieve this goal by supplying student (users) with helpful videos, assignments and feedback on the finished assignments. We will also try to get students to help each other, by implementing a forum.
    This forum can also be used for questions about things that might still be unclear after watching the videos or questions that are not related to a course at all. 
    There will also be a bulletin board in our application, on which a student can see events that are happening in which they can take part.
    It's important to see progress as a student which is why we will also include a profile page on which a user can easily see what courses have been completed and what courses are being followed on at the moment. It's also possible to bookmark certain topics or events so they appear on your profile for easy accessibility.
    
    In order to help visualize our idea we have made mockups, as you can see below:


### Home page

    On their home page the users can see selected snippets from the courses, the events and the forum posts - they can scroll through and open them. Through the header they can search for specific content in the application, go to their profile page (or be prompted to sign in with GitHub if they haven't), or access all other pages through the dropdown menu (the page they are on currently is highlighted) with the extra options for language selection and signing out.

![alt home1](mockups/tablet/home1.png)
![alt home2](mockups/tablet/home2.png)
![alt home3](mockups/tablet/home3.png)
![alt menu](mockups/tablet/menu.png)
![alt sign-up-in](mockups/tablet/sign_up-in.png)

### Course pages

    On the course page the users can see a selection of different courses split by category which they can filter and search through. If they select a specific course they are forwarded to the page of that course containing the course's overview with the option to bookmark it, as well as the contents which contain all videos/assignments (with the progress being saved and displayed with checkmarks for already done/watched and underline for currently active). For both video and course assignment a forum topic is automatically generated (the teacher can pick questions and answers from there to add to the q&a section) and the student can access it through the corresponding icon, they can also report them if they deem them inappropriate. The assignments contain a short description and a link to the GitHub repository which contains further information and with which the students can interact with to do the assignment.

![alt course1](mockups/tablet/course_page.png)
![alt course-page-filter](mockups/tablet/course_page-filter.png)
![alt course-student](mockups/tablet/courses_student.png)
![alt course-student2](mockups/tablet/courses_student2.png)

### Event pages

    On the events page the users can see all upcoming events with their names, dates, and locations along with a picture representing them. They can choose to follow a specific event which would display it in their profile page.

![alt events-page](mockups/tablet/events-page.png)

### Forum pages

    On the forum page the users can browse through posts created by other users separated on different categories which they can join. They can create a topic of their own or comment and reply to others in already existing topics. They can also upvote/downvote topics and comments as well as report anything they deem inappropriate.

![alt forum1](mockups/tablet/forum1.png)
![alt forum2](mockups/tablet/forum2.png)
![alt forum3](mockups/tablet/forum3.png)
![alt course-creation](mockups/tablet/course_creation2.png)

### Profile page

    On the profile page the normal users can see their course progress as well as the ones they've bookmarked, the events they're following and posts from the topics they've joined. They have several options the main one being customizing the layout of their page which allows them to switch the different sections around and to hide/show them.

![alt profile1](mockups/tablet/profile_student-1.png)
![alt profile2](mockups/tablet/profile_student-2.png)
![alt profile3](mockups/tablet/profile_student-3.png)
![alt profile4](mockups/tablet/profile_student-4.png)

### Teacher pages

    The teachers have several screens that differentiate them from normal users. When they open one of the auto-generated forum posts connected to their course's video/assignment they can pick and choose comments and replies to mark as Questions and Answers which would put them in the Q&A section of that video/assignment. On their profile they can see their courses with some basic statistics on each one as well as an indication if they have been blocked. They also can create/edit a course by providing the needed information, connecting it to their GitHub course repository and filling it with videos/assignments separated on different chapters.

![alt forum4](mockups/tablet/forum4.png)
![alt profile-teacher](mockups/tablet/profile_teacher-1.png)
![alt course-creation](mockups/tablet/course_creation1.png)
![alt course-creation](mockups/tablet/course_creation3.png)

### Admin pages

    The admin has a unique page header layout split between users (accounts, reports) and contents (course, events, forum). In the accounts page they can view all users sorted by different factors like their reputation (number of thumbs up received - number of thumbs down received) and give them different roles (student, teacher, admin - which is unchangeable, and moderator - who are to have the same functions as the admin outside of managing the users roles), as well as block the user from posting content (the padlock). The reports page contains entries for all reports received grouped by post - the admin can choose to act upon the report by blocking the content and/or the user, view the content or hide the report. In the content part the admin is presented with versions of the pages similar to the normal user ones but with the addition of the block/unblock functionality and the creation of topics for the forum, and categories for the events and for the courses.

![alt admin-users-accounts](mockups/tablet/admin_users-accounts.png)
![alt admin-users-reports](mockups/tablet/admin_users-reports.png)
![alt admin-content-courses](mockups/tablet/admin_content-courses.png)
![alt admin-content-events](mockups/tablet/admin_content-events.png)
![alt admin-content-forum](mockups/tablet/admin_content-forum.png)
![alt admin-menu](mockups/tablet/admin-menu.png)

![alt admin-content-courses-buttons](mockups/tablet/admin_content-courses-buttons.png)
![alt admin-content-courses-buttons](mockups/tablet/admin_content-events-buttons.png)
![alt admin-content-forum-button](mockups/tablet/admin_content-forum-button.png)