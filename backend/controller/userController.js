var axios = require('axios');
var User = require('../models/user');
var Course = require('../models/course/course');
var Chapter = require('../models/course/chapter');
var Assignment = require('../models/course/assignment');
var Video = require('../models/course/video');
var FAQ = require('../models/course/faq');
var Bookmark = require('../models/course/bookmark');

var jwt = require('jsonwebtoken');

let JWT_SECRET = require('../database/jwt-secret');

/**
 * User controler handles all operations regarding sign-up/login bookmarking courses and events.
 * Sign-up/login is handled through git hubs OAuth api
 * Source and documentation is found at:
 * https://developer.github.com/apps/building-oauth-apps/authorizing-oauth-apps/
 */
module.exports = (app) => {

    app.get('/user/signin/handler', (req, res) => {
        //the query now contains a code, we need that code to get an access token
        let jwtoken;
        let code = req.query.code;

        //post code to github api
        axios.post('https://github.com/login/oauth/access_token',
            {
                client_id: 'b4a0d7ce8cc604c742e1',
                client_secret: '65b72f6fb9c4236730174e1229b3e8e160a93c73',
                code: code
            },
            {
                headers: {
                    Accept: 'application/json'
                }
            }).then((response) => {
            //in this response we get the access token
            let access_token = response.data.access_token;

            //now we can request all the user information from the github api
            axios.get('https://api.github.com/user', {
                headers: {
                    Authorization: 'token ' + access_token
                }
            }).then((rsp) => {
                //in here we get the user information from github

                //check if he exists
                let user;
                const url = 'http://' + window.location.hostname + ':3000';
                axios.get(url + '/users/' + rsp.data.id)
                    .then(dbUser => {
                        //user is found in our database
                        user = dbUser.data;
                    })
                    .catch(err => {
                        //user does not exist in our database -> create him
                        user = new User(rsp.data.login, rsp.data.id, 1);
                        let userValues = [user.id, user.username, user.first_name, user.last_name, user.role_id];

                        app.db.query(`INSERT INTO users values (?,?,?,?,?) `, userValues, (err, results) => {
                            console.log(err);
                            console.log(results);
                        });
                    })
                    .then(() => {
                        //now that we have a user, let's store him in a jwt token so the frontend can work with it

                        // log the user in to our system -- THIS IS THE HAPPY FLOW
                        jwt.sign({user: user}, JWT_SECRET, (err, token) => {
                            jwtoken = "Bearer " + token;
                            res.json({
                                token: jwtoken
                            });
                        });
                    });

                // the following catch clauses mean something has gone wrong when requesting something to the github api.
                // (maybe the code was expired because a user was messing around)
                // in that case, we send back an undefined token, which will be caught in the Loading screen component (where
                // login request was made initially)
            }).catch(() => {
                res.json({
                    token: jwtoken
                });
            });
        }).catch(() => {
            res.json({
                token: jwtoken
            });
        });

    });
};