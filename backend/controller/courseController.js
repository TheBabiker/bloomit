var Group = require('../models/course/group');
var Course = require('../models/course/course');
var Chapter = require('../models/course/chapter');
var Assignment = require('../models/course/assignment');
var Video = require('../models/course/video');
var FAQ = require('../models/course/faq');
var Bookmark = require('../models/course/bookmark');
var User = require('../models/user');

module.exports = (app) => {
    app.get('/courses', (req, res, next) => {
        let courses = [];
        app.db.query("SELECT subjects.id, subjects.name, subjects.group_id, subjects.overview, subjects.lecturer_id FROM subjects", function (err, result) {
            if (err) throw err;
            for (let i = 0; i < result.length; i++) {
                let course = new Course(result[i].id, result[i].name, result[i].group_id, result[i].overview, result[i].lecturer_id);
                courses.push(course);
            }
            res.json(courses);
        });
    });

    app.get('/course/:id', (req, res, next) => {
        let course;
        app.db.query("SELECT subjects.id, subjects.name, subjects.group_id, subjects.overview, subjects.lecturer_id FROM subjects WHERE id = " + req.params.id, function (err, result) {
            if (err) throw err;
            course = new Course(result[0].id, result[0].name, result[0].group_id, result[0].overview, result[0].lecturer_id);
            res.json(course);
        });
    });

    app.get('/course/:id/faq', (req, res, next) => {
        let faqs = [];
        app.db.query("SELECT faqs.id, faqs.question, faqs.answer, faqs.subject_id FROM faqs WHERE faqs.subject_id = " + req.params.id, function (err, result) {
            if (err) throw err;
            for (let i = 0; i < result.length; i++) {
                let faq = new FAQ(result[i].question, result[i].answer, result[i].subject_id);
                faqs.push(faq);
            }
            res.json(faqs);
        });
    });

    app.delete('/course/faq/delete/:id', (req, res, next) => {
        let query = "DELETE FROM faqs WHERE id = " + req.params.id;
        app.db.query(query, function(err, result){
            if(err) throw err;
            res.json("Deleted faq!")
        })
    })

    app.put('/faq/update/:id', (req, res, next) => {
        let query = "UPDATE faqs SET question='"+ req.body.question +"', answer='" + req.body.answer + "' WHERE id = " + req.params.id;
         app.db.query(query, function(err, result){
            if(err) throw err;
            res.json("Changed!")
        })
    })

    app.post('/course/faq/push/:courseId', (req, res, next) => {
        let query = "INSERT INTO `faqs`(`question`, `answer`, `subject_id`) VALUES ('-', '-'," + req.params.courseId + ")";
        app.db.query(query, function(err, result){
            if(err) throw err;
            console.log("Inserted!")
        })
    })

    app.get('/course/:id/chapters', (req, res, next) => {
        let chapters = [];
        app.db.query("SELECT chapters.id, chapters.name as chapterName, chapters.subject_id as courseId FROM chapters WHERE subject_id = " + req.params.id, function (err, result) {
            if (err) throw err;
            for (let i = 0; i < result.length; i++) {
                let chapter = new Chapter(result[i].id, result[i].chapterName, result[i].courseId);
                chapters.push(chapter);
            }
            res.json(chapters);
        });
    });

    //Videos
    app.get('/course/:courseId/chapters/:chapterId/videos', (req, res, next) => {
        let videos = [];
        app.db.query("SELECT videos.id, videos.title, videos.link, videos.chapter_id FROM videos WHERE videos.chapter_id = " + req.params.chapterId, function (err, result) {
            if (err) throw err;
            for (let i = 0; i < result.length; i++) {
                let video = new Video(result[i].id, result[i].title, result[i].link, result[i].chapter_id);
                videos.push(video);
            }
            res.json(videos);
        });
    });

    app.post('/course/chapters/videos/:chapterId/:title', (req, res, next) => {
        let query = "INSERT INTO videos SET title='"+req.params.title+"', chapter_id="+req.params.chapterId+" , link = '"+req.body.videoLink+ "'";
        app.db.query(query, function(err, result){
            if(err) throw err;
            console.log("Inserted!")
        })
    })

    app.delete('/course/chapters/videos/:chapterId/:videoId', (req, res, next) => {
        let query = "DELETE FROM videos WHERE id = " + req.params.videoId + " AND chapter_id = " + req.params.chapterId;
        app.db.query(query, function(err, result){
            if(err) throw err;
            res.json("deleted video!")
        })
    })

    app.put('/course/chapters/videos/:chapterId/:id/:videoTitle', (req, res, next) => {
        let query = "UPDATE videos SET chapter_id = " + req.params.chapterId + ", title = '" + req.params.videoTitle+ "', link = '" + req.body.videoLink + "' "
        + " WHERE chapter_id = " + req.params.chapterId + " AND id = " + req.params.id;
         app.db.query(query, function(err, result){
            if(err) throw err;
            res.json("Changed!")
        })
    })
    
    //Assignments
    app.get('/course/:courseId/chapters/:chapterId/assignments', (req, res, next) => {
        let assignments = [];
        app.db.query("SELECT assignments.id, assignments.name, assignments.chapter_id, assignments.description, assignments.github_link FROM assignments WHERE assignments.chapter_id = " + req.params.chapterId, function (err, result) {
            if (err) throw err;
            for (let i = 0; i < result.length; i++) {
                let assignment = new Assignment(result[i].id, result[i].name, result[i].chapter_id, result[i].description, result[i].github_link);
                assignments.push(assignment);
            }
            res.json(assignments);
        });
    });

    app.post('/course/chapters/assignments/:chapterId/:name', (req, res, next) => {
        let query = "INSERT INTO assignments SET chapter_id="+req.params.chapterId+" , name ='"+req.params.name + "'";
        app.db.query(query, function(err, result){
            if(err) throw err;
            res.json("Inserted!");
        })
    })

    app.delete('/course/chapters/assignments/:chapterId/:assignmentId', (req, res, next) => {
        let query = "DELETE FROM assignments WHERE id = " + req.params.assignmentId;
        app.db.query(query, function(err, result){
            if(err) throw err;
            res.json("Deleted assignment!")
        })
    })

    app.put('/course/chapters/assignments/edit/:chapterId/:assignmentId/:name/:description', (req, res, next) => {
        let query = "UPDATE assignments SET chapter_id = " + req.params.chapterId + ", name = '" + req.params.name
        + "', description = '" + req.params.description + "', github_link = '" + req.body.github_link + 
        "' WHERE id = " + req.params.assignmentId + " AND chapter_id = " + req.params.chapterId;
         app.db.query(query, function(err, result){
            if(err) throw err;
            res.json("Changed!")
        })
    })

    app.post('/course/:courseId/:chapterName/', (req, res, next) => {
        if(req.params.chapterName == null){
            res.status(400).json("Not all information is provided")
        }
        let sqlLine = 'INSERT INTO chapters(name, subject_id) VALUES (?,?)';
        let parameters = [
            req.params.chapterName,
            req.params.courseId
        ];
        app.db.query(sqlLine, parameters, function(err, result){
            if(err) throw err;
        })
    });

    app.post('/course/:courseName/:overview/:lecturerId/:groupId', (req, res, next) => {
        if (req.params.courseName == null || req.params.overview == null || req.params.lecturerId == null || req.params.groupId == null) {
            res.status(400).json("Not all information is provided")
        }

        let newCourse;
        let sqlLine = 'INSERT INTO subjects(name, group_id, overview, lecturer_id) VALUES (?,?,?,?)';
        let parameters = [
            req.params.courseName,
            req.params.groupId,
            req.params.overview,
            req.params.lecturerId
        ];
        app.db.query(sqlLine, parameters, function(err, result){
            if(err) throw err;
        })
    });

    app.put('/course/:courseId/:chapterName/:id', (req, res, next) => {
        let query = "UPDATE chapters SET name='"+req.params.chapterName+"', subject_id="+req.params.courseId+" WHERE chapters.id = "+req.params.id;
        app.db.query(query, function(err, result){
            if(err) throw err;
            console.log("Updated!")
        })
    })

    app.put('/course/short/edit/rename/:courseName/:overview/:id',(req, res, next) =>{
        let referenceId = parseInt(req.params.id);
        let query = "UPDATE subjects SET name='"+req.params.courseName+"', overview='"+req.params.overview+"' WHERE subjects.id =" + referenceId;
        app.db.query(query, function(err, result){
            console.log(err)
            if(err) throw err;
            console.log("Updated!")
        })
    });

    app.put('/course/:courseName/:overview/:lecturerId/:groupId/:id',(req, res, next) =>{
        let parameters = {
            name: req.params.courseName,
            group_id: parseInt(req.params.groupId),
            overview: req.params.overview,
            lecturer_id: parseInt(req.params.lecturerId),
        };
        let referenceId = parseInt(req.params.id);
        let sqlLine = "UPDATE subjects SET ? WHERE ?";
        let whereStatement = "WHERE id =" + referenceId;
        let query = "UPDATE subjects SET name='"+req.params.courseName+"', group_id="+req.params.groupId+", overview='"+req.params.overview+"', lecturer_id="+req.params.lecturerId+" WHERE subjects.id = "+req.params.id;
        app.db.query(query, function(err, result){
            console.log(err)
            if(err) throw err;
            console.log("Updated!")
        })
    });

    app.delete('/course/chapter/:id/', (req, res, next) => {
        let query = "DELETE FROM chapters WHERE chapters.id = "+req.params.id;
        app.db.query(query, function(err, result){
            if(err) throw err;
            console.log(result.affectedRows + "row(s) deleted!")
        })
    });

    app.delete('/course/:id/', (req, res, next) => {
        let query = "DELETE FROM subjects WHERE subjects.id = "+req.params.id;
        app.db.query(query, function(err, result){
            if(err) throw err;
            console.log(result.affectedRows + "row(s) deleted!")
        })
    });

    //Bookmarks
    app.get('/:courseId/:userId/bookmark', (req, res, next) => {
        let bookmarks = []
        app.db.query("SELECT course_id, user_id FROM bookmarks WHERE course_id = " + req.params.courseId + " AND user_id = " + req.params.userId, function (err, result) {
            if (err) throw err;
            
            let isBookmarked = false;
            if (result.length > 0) {
                isBookmarked = true;
            }
            let bookmark = new Bookmark(isBookmarked);
            bookmarks.push(bookmark);
            res.json(bookmark);
        });
    });

    app.post('/:courseId/:userId/bookmark/post', (req, res, next) => {
        let sqlLine = 'INSERT INTO bookmarks(course_id, user_id) VALUES (?,?)';
        let parameters = [
            req.params.courseId,
            req.params.userId
        ];
        app.db.query(sqlLine, parameters, function(err, result){
            if(err) throw err;
            res.json("inserted bookmark!")
        })
    });

    app.delete('/:courseId/:userId/bookmark/delete', (req, res, next) => {
        let query = "DELETE FROM bookmarks WHERE course_id = " + req.params.courseId + " AND user_id = " + req.params.userId;
        app.db.query(query, function(err, result){
            if(err) throw err;
            res.json("deleted bookmark!")
        })
    });

    // GROUPS
    app.get('/groups', (req, res, next) => {
        let groups = [];
        app.db.query("SELECT id, name FROM groups", function (err, result) {
            if (err) throw err;
            for (let i = 0; i < result.length; i++) {
                let group = new Group(result[i].id, result[i].name);
                groups.push(group);
            }
            res.json(groups);
        });
    })

    app.get('/get/bookmarked/courses/:userId', (req, res, next) => {
        let courses = [];
        let userId = req.params.userId
        app.db.query("SELECT subjects.id, subjects.name, subjects.group_id, subjects.overview, subjects.lecturer_id FROM subjects INNER JOIN bookmarks ON bookmarks.course_id = subjects.id AND bookmarks.user_id = " + userId, function (err, result) {
            if (err) throw err;
            for (let i = 0; i < result.length; i++) {
                course = new Course(result[i].id, result[i].name, result[i].group_id, result[i].overview, result[i].lecturer_id);
                courses.push(course);
            }
            res.json(courses);
        });
    });

    // USERS
    app.get('/user/username/:userId', (req, res, next) => {
        let userId = req.params.userId
        app.db.query("SELECT users.username FROM users WHERE users.id = " + userId, function (err, result) {
            if (err) throw err;
            let username = result[0].username
            res.json(username);
        });
    });
}