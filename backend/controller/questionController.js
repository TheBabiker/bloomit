class QuestionController {
    /**
     * A constructor to inlitalize the db object with the corresponding collection.
     * @param {db} db of firebase
     */
    // constructor(db) {
    //     this.db = db.collection('Question');
    // }

    constructor() {

    }

    setDb(db) {
        this.db = db;
    }

     static sortQuestions(orderBy, sortBy, arr) {

        let c;
        let d;

        if (orderBy == "desc") {
            c = -1;
            d = 1;
        } else if (orderBy == "asc") {
            c = 1;
            d = -1
        }
        if (sortBy == "rating") {

            arr.sort((a, b) => (a.Rating < b.Rating ? c : d));
        } else if (sortBy == "date") {
            arr.sort((a, b) => (a.DateAsked < b.DateAsked ? c : d));
        }

        return arr;
    }

     static dateDiffInDays(a, b) {
        const _MS_PER_DAY = 1000 * 60 * 60 * 24;

         // Discard the time and time-zone information.
        const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
        const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

        return Math.floor((utc2 - utc1) / _MS_PER_DAY);
    }
}
module.exports = QuestionController;


// function sortQuestions(orderBy, sortBy, arr) {
//
//     let c;
//     let d;
//
//     if (orderBy == "desc") {
//         c = -1;
//         d = 1;
//     } else if (orderBy == "asc") {
//         c = 1;
//         d = -1
//     }
//     if (sortBy == "rating") {
//
//         arr.sort((a, b) => (a.Rating < b.Rating ? c : d));
//     } else if (sortBy == "date") {
//         arr.sort((a, b) => (a.DateAsked < b.DateAsked ? c : d));
//     }
//
//     return arr;
// }
//
// function dateDiffInDays(a, b) {
//     const _MS_PER_DAY = 1000 * 60 * 60 * 24;
//
//     // Discard the time and time-zone information.
//
//     if(a === undefined || b === undefined) {
//         console.log("Values are undefined");
//         return ;
//     }
//
//     const utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
//     const utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
//
//     return Math.floor((utc2 - utc1) / _MS_PER_DAY);
// }
//
// module.exports = dateDiffInDays();
// module.exports = sortQuestions();
//

