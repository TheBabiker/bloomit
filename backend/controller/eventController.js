var Event = require('../models/event');
const filePath = "";

module.exports = (app) => {
    app.get('/events', (req, res, next) => {
        let events = [];

        getAllEvents(req, res, function(events2) {
            getAllAttendees(function(attendees) {
                getAllFiles(function(files) {
                    for (let i = 0; i < events2.length; i++) {
                        events.push(events2[i])
                        for (let j = 0; j < attendees.length; j++) {
                            if (events[i].id == attendees[j].event_id) {
                                events[i].attendees.push(attendees[j].username)
                            }
                        }
                        for (let j = 0; j < files.length; j++) {
                            if (events[i].id == files[j].event_id) {
                                if (files[j].file_type === 'image') {
                                    events[i].pictures.push(req.protocol + '://' + req.get('host') + "/" + filePath + files[j].file_name)
                                } else if (files[j].file_type === 'url') {
                                    events[i].pictures.push(files[j].file_name)
                                }
                            }
                        }
                    }
                    if (events.length != 0) {
                        res.status(200).json(events);
                    } else res.status(404).json('No matching results.')
                })
            })
        })
    });

    app.get('/events/:id', (req, res, next) => {
        let tempEvents = []
        let events = [];

        getAllEvents(req, res, function(events2) {
            getAllAttendees(function(attendees) {
                getAllFiles(function(files) {
                    for (let i = 0; i < events2.length; i++) {
                        tempEvents.push(events2[i])
                        for (let j = 0; j < attendees.length; j++) {
                            if (tempEvents[i].id == attendees[j].event_id) {
                                tempEvents[i].attendees.push(attendees[j].username)
                            }
                        }
                        for (let j = 0; j < files.length; j++) {
                            if (tempEvents[i].id == files[j].event_id) {
                                if (files[j].file_type === 'image') {
                                    tempEvents[i].pictures.push(req.protocol + '://' + req.get('host') + "/" + filePath + files[j].file_name)
                                } else if (files[j].file_type === 'url') {
                                    tempEvents[i].pictures.push(files[j].file_name)
                                }
                            }
                        }       
                        if (tempEvents[i].id == req.params.id) {
                            events.push(tempEvents[i])
                        }
                    }
                    if (events.length != 0) {
                        res.status(200).json(events);
                    } else res.status(404).json('No matching results.')
                })
            })
        })
    });

    app.post('/events', (req, res, next) => {
        if (req.body.title == null || req.body.description == null || req.body.location == null || req.body.time == null || req.body.date == null || req.body.picture == null || req.body.pictureType == null || req.body.category == null || req.body.numberOfPicture == null) {
            res.status(400).json("Not all information is provided")
        } else {
            if (req.body.time.length > 5) res.status(400).json("Time format should be HH:MM")
            let categoryID;
            if (toLowerCase(req.body.category) == toLowerCase("meetup")) {
                categoryID = 1;
            } else if (toLowerCase(req.body.category) == toLowerCase("charity")) {
                categoryID = 2;
            } else if (toLowerCase(req.body.category) == toLowerCase("workshop")) {
                categoryID = 3;
            } else if (toLowerCase(req.body.category) == toLowerCase("party")) {
                categoryID = 4;
            } else {
                categoryID =5;
            }
            let sql1 = `INSERT INTO events (name, description, location, category_id, time, date) VALUES (?, ?, ?, ?, ?, ?)`;
            let sql2 = `INSERT INTO files (name, type) VALUES ?`;
            let sql3 = `INSERT INTO event_files (event_id, file_id) VALUES ?`;
            let params1 = [
                req.body.title,
                req.body.description,
                req.body.location,
                categoryID,
                req.body.time,
                req.body.date
            ]
            let params2 = [
                [req.body.picture, req.body.pictureType],
            ]
            if (req.body.numberOfPicture > 1) {
                for (let i = 2; i <= req.body.numberOfPicture; i++) {
                    params2.push([req.body[`picture` + i], req.body[`pictureType` + i]])
                }
            }
            app.db.query(sql1, params1, function (err, result) {
                if (err) throw err;
                let insertedEventID = result.insertId;
                app.db.query(sql2, [params2], function (err, result) {
                    if (err) throw err;
                    let params3 = []
                    for (let i = 0; i < req.body.numberOfPicture; i++) {
                        params3.push([[insertedEventID], result.insertId + i])
                    }
                    app.db.query(sql3, [params3], function (err, result) {
                        if (err) throw err;
                        res.status(201).json("Add new event successfully");
                    });
                });
            });
        }
    });

    app.post('/events/:id', (req, res, next) => {
        if (req.body.username == null) {
            res.status(401).json("Please login to subscribe to event")
        } else {
            let sql1 = `SELECT events.id FROM events WHERE events.id = ?`;
            let sql2 = `SELECT users.id FROM users WHERE users.username = ?`;
            let sql3 = `SELECT event_users.event_id, event_users.user_id FROM event_users WHERE event_users.event_id = ? AND event_users.user_id = ?`;
            let sql4 = `INSERT INTO event_users (event_id, user_id) VALUES (?, ?)`;
            let sql5 = `DELETE event_users FROM event_users WHERE event_users.event_id = ? AND event_users.user_id = ?`;
    
            app.db.query(sql1, req.params.id, function (err, result) {
                if (err) throw err;
                if (result.length === 0) res.status(404).json("There is no event with id: " + req.params.id);
                else
                app.db.query(sql2, req.body.username, function (err, result) {
                    if (err) throw err;
                    if (result.length === 0) res.status(404).json("There is no user with username: " + req.body.username);
                    else {
                        let params = [
                            req.params.id,
                            result[0].id
                        ]
                        app.db.query(sql3, params, function (err, result) {
                            if (err) throw err;
                            if (result.length === 0) {
                                app.db.query(sql4, params, function (err, result) {
                                    if (err) throw err;
                                    res.status(201).json("Subscribe to event successfully");
                                });
                            }
                            else {
                                app.db.query(sql5, params, function (err, result) {
                                    if (err) throw err;
                                    res.status(201).json("Unsubscribe to event successfully");
                                });
                            }
                        });
                    }
                });
            });
        }
    });

    app.put('/events/:id', (req, res, next) => {
        if (req.body.title == null || req.body.description == null || req.body.location == null || req.body.time == null || req.body.date == null || req.body.picture == null || req.body.pictureType == null || req.body.category == null) {
            res.status(400).json("Not all information is provided")
        } else {
            if (req.body.time.length > 5) res.status(400).json("Time format should be HH:MM")
            else {
                let category_id;
                if (toLowerCase(req.body.category) == toLowerCase("meetup")) {
                    category_id = 1;
                } else if (toLowerCase(req.body.category) == toLowerCase("charity")) {
                    category_id = 2;
                } else if (toLowerCase(req.body.category) == toLowerCase("workshop")) {
                    category_id = 3;
                } else if (toLowerCase(req.body.category) == toLowerCase("party")) {
                    category_id = 4;
                } else {
                    category_id =5;
                }
                let sql1 = `SELECT events.id FROM events WHERE events.id = ?`;
                let sql2 = `UPDATE events SET events.name = ?, events.description = ?, events.location = ?, events.category_id = ?, events.time = ?, events.date = ? WHERE events.id = ?`;
                let sql3 = `SELECT * FROM event_files WHERE event_files.event_id = ?`;
                let sql4 = `UPDATE files SET files.name = ?, files.type = ? WHERE files.id = ?`;

                let params2 = [
                    req.body.title,
                    req.body.description,
                    req.body.location,
                    category_id,
                    req.body.time,
                    req.body.date,
                    req.params.id
                ]
        
                app.db.query(sql1, req.params.id, function (err, result) {
                    if (err) throw err;
                    if (result.length == 0) res.status(404).json("There is no event with id: " + req.params.id);
                    else {
                        app.db.query(sql2, params2, function (err, result) {
                            if (err) throw err;
                            app.db.query(sql3, req.params.id, function (err, result) {
                                if (err) throw err;
                                let fileID = [];
                                for (let i = 0; i < result.length; i++) {
                                    fileID.push({event_id: result[i].event_id, file_id: result[i].file_id})
                                }
                                app.db.query(sql4, [req.body.picture, req.body.pictureType, fileID[0].file_id], function (err, result) {
                                    if (err) throw err;
                                    if (fileID.length > 1) {
                                        for (let i = 2; i <= fileID.length; i++) {
                                            app.db.query(sql4, [req.body[`picture` + i], req.body[`pictureType` + i], fileID[i-1].file_id], function (err, result) {
                                                if (err) throw err;
                                            });
                                        }
                                    }
                                    res.status(200).json("Edit event successfully");
                                });
                            });
                        });
                    }
                })
            }
        }
    });

    app.delete('/events/:id', (req, res, next) => {
        let sql1 = `SELECT events.id FROM events WHERE events.id = ?`;
        let sql2 = `DELETE events, event_files FROM events INNER JOIN event_files on event_files.event_id = events.id INNER JOIN files on event_files.file_id = files.id WHERE events.id = ?`;

        app.db.query(sql1, req.params.id, function (err, result) {
            if (err) throw err;
            if (result.length == 0) res.status(404).json("There is no event with id: " + req.params.id);
            else
            app.db.query(sql2, req.params.id, function (err, result) {
                if (err) throw err;
                res.status(200).json("Delete event successfully");
            });
        })
    });

    function sortArrayById(arr) {
        arr.sort(function (a, b) {
            return b.id - a.id;
        })
    }

    function getAllEvents(req, res, cb) {
        let events = [];
        let sql = `SELECT events.id, events.name, events.description, events.time, events.date, events.location, event_categories.name as category FROM events INNER JOIN event_categories on events.category_id = event_categories.id`;
        if (req.query.category != null) {
            switch (req.query.category) {
                case 'meetup': sql += " WHERE events.category_id = 1"; break;
                case 'charity': sql += " WHERE events.category_id = 2"; break;
                case 'workshop': sql += " WHERE events.category_id = 3"; break;
                case 'parties': sql += " WHERE events.category_id = 4"; break;
                default: res.status(404).json('No matching results.');
            }
        }
    
        app.db.query(sql, function (err, result) {
            if (err) throw err;
            for (let i = 0; i < result.length; i++) {
                // fill model
                let eventDate = new Date(result[i].date);
                let event = new Event(result[i].id, result[i].name, result[i].description, result[i].location, result[i].time,
                    (eventDate.getDate() + "-" + (eventDate.getMonth() + 1) + "-" + eventDate.getFullYear()),
                    [], result[i].category, []);
                events.push(event);
            }
            sortArrayById(events);
            cb(events);
        });
    }

    function getAllBookmarkedEvents(req, res, cb) {
        let events = [];
        let sql = `SELECT events.id, events.name, events.description, events.time, events.date, events.location FROM events INNER JOIN event_users ON event_users.event_id = events.id AND event_users.user_id = ` + req.params.userId;
    
        app.db.query(sql, function (err, result) {
            if (err) throw err;
            for (let i = 0; i < result.length; i++) {
                // fill model
                let eventDate = new Date(result[i].date);
                let event = new Event(result[i].id, result[i].name, result[i].description, result[i].location, result[i].time,
                    (eventDate.getDate() + "-" + (eventDate.getMonth() + 1) + "-" + eventDate.getFullYear()),
                    [], 0, []);
                events.push(event);
            }
            sortArrayById(events);
            cb(events);
        });
    }

    function getAllAttendees(cb) {
        let sql2 = `SELECT events.id AS event_id, users.username FROM events INNER JOIN event_users ON events.id = event_users.event_id INNER JOIN users ON users.id = event_users.user_id`;
        let events = []
        app.db.query(sql2, function (err, result) {
            if (err) throw err;
            for (let i = 0; i < result.length; i++) {
                events.push({event_id: result[i].event_id, username: result[i].username})
            }
            cb(events);
        });
    }

    function getAllFiles(cb) {
        let sql2 = `SELECT events.id AS event_id, files.name AS file_name, files.type AS file_type FROM events INNER JOIN event_files on events.id = event_files.event_id INNER JOIN files ON files.id = event_files.file_id`;
        let events = []
        app.db.query(sql2, function (err, result) {
            if (err) throw err;
            for (let i = 0; i < result.length; i++) {
                events.push({
                    event_id: result[i].event_id, 
                    file_name: result[i].file_name, 
                    file_type: result[i].file_type
                })
            }
            cb(events);
        });
    }

    function toLowerCase(str) {
        return str.toLowerCase;
    }

    //SELECT * FROM events INNER JOIN event_users ON event_users.event_id = events.id AND event_users.user_id = 47715382

    app.get('/events/bookmarked/:userId', (req, res, next) => {
        let events = [];

        getAllBookmarkedEvents(req, res, function(events2) {
            getAllAttendees(function(attendees) {
                getAllFiles(function(files) {
                    for (let i = 0; i < events2.length; i++) {
                        events.push(events2[i])
                        for (let j = 0; j < attendees.length; j++) {
                            if (events[i].id == attendees[j].event_id) {
                                events[i].attendees.push(attendees[j].username)
                            }
                        }
                        for (let j = 0; j < files.length; j++) {
                            if (events[i].id == files[j].event_id) {
                                if (files[j].file_type === 'image') {
                                    events[i].pictures.push(req.protocol + '://' + req.get('host') + "/" + filePath + files[j].file_name)
                                } else if (files[j].file_type === 'url') {
                                    events[i].pictures.push(files[j].file_name)
                                }
                            }
                        }
                    }
                    if (events.length != 0) {
                        res.status(200).json(events);
                    } else res.status(404).json('No matching results.')
                })
            })
        })
    });

}