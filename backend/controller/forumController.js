// //import User from "../models/user";

// /**
//  * Controller class example.
//  * The reason for this controller is to group the functionalities together.
//  * This will provide a more clear structure and overview of code.
//  */
// class forumController {

//     constructor(db) {
//         this.db = db.collection('Question');
//     }

//     static postQuestion(question){
//         this.db.add(question).then(() => {
//             return true;
//         }).catch(err => {
//             console.log(err);
//             return false;
//         })
//     }

//     getQuestions() {

//     }



// }

exports.getRating = function(req, rsp) {
    var up = 0;
    var down = 0;
    req.db.query('SELECT COUNT(*) as rating FROM user_vote WHERE '+req.params.item+'=? and type=? GROUP BY '+req.params.item, 
    [req.params.id, true], function(err, row) {
        if(row.length > 0) {
            up = row[0].rating;
        }
        req.db.query('SELECT COUNT(*) as rating FROM user_vote WHERE '+req.params.item+'=? and type=? GROUP BY '+req.params.item, 
        [req.params.id, false], function(err, lrow) {
            if(lrow.length > 0) {
                down = lrow[0].rating;
            }
            rsp.status(200).json(up - down);
        })
    })
}

exports.getRatingWithPost = function(req, post, item, rsp) {
    var up = 0;
    var down = 0;
    req.db.query('SELECT COUNT(*) as rating FROM user_vote WHERE '+item+'=? and type=? GROUP BY '+item, 
    [req.params.id, true], function(err, row) {
        if(row.length > 0) {
            up = row[0].rating;
        }
        req.db.query('SELECT COUNT(*) as rating FROM user_vote WHERE '+item+'=? and type=? GROUP BY '+item, 
        [req.params.id, false], function(err, lrow) {
            if(lrow.length > 0) {
                down = lrow[0].rating;
            }
            var d = Object.assign({}, post[0], {rating: (up-down)})
            rsp.status(200).json(d);
        })
    })
}
