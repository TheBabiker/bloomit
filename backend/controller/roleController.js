 /**
 * Controller class example.
 * The reason for this controller is to group the functionalities together.
 * This will provide a more clear structure and overview of code.
 */
class RoleController {
    /**
     * A constructor to inlitalize the db object with the corresponding collection.
     * @param {FirebaseFirestore.Firestore} db of firebase
     */ 
    constructor(db) {
        // this.db = db.collection('Role');
    }

    /**
     * Add a role
     * @param {role} role to add
     */
    addRole(role) {
        role.Id = this.getLastId() + 1;
        this.db.doc(role.Name).set(role);
    }

    /**
     * Quick function to get the last ID.
     * Might not be the most optimal but this is what I have found so far.
     * If you found a better method please tell us!
     */
    getLastId() {
        var roles = this.getRoles();
        return roles[roles.length-1].Id;
    }
}

module.exports = RoleController;