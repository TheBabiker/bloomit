/**
 * ATTENTION!!!
 * Please take a close look at classdiagram.md!!
 * To prevent mistakes in the structure and the creation of duplicate collections and docs.
 * Thank you and good luck!
 */

var express = require("express");
var jwt = require('jsonwebtoken');
const JWT_SECRET = require('./database/jwt-secret');
var User = require('./models/user');

const app = express();

//get db from external file (did this so its reusable in other api path files)
let db = require('./database/database');
var cors = require('cors');
app.db = db;

app.db.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

//use header to allow requests from frontend (actually, from everywhere, authentication to be added later)
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Authorization");
    req.db = db;
    next();
});
app.use(express.json());
app.use(cors());
/* Controller imports */
var RoleController = require('./controller/roleController');
require('./controller/eventController')(app);
require('./controller/courseController')(app);

//include the sign-in path
require('./controller/userController')(app);
app.use(require('./routers/forumRouter'));
app.use(function(req,rsp,next){
	req.db = db;
	next();
});

app.use(require('./routers/eventRouter'));

app.listen(3000, () => {
    console.log("Server running on port 3000");
});

module.exports = app;

//used to check if there's a token, if so the token is extracted from the header and placed in req.token
function verifyToken(req, res, next) {
    let authHeader = req.headers['authorization'];
    if (typeof authHeader !== "undefined") {
        //token found in header
        req.token = authHeader.split(' ')[1];
        jwt.verify(req.token, JWT_SECRET, (err, tokenData) => {
            if (err) {
                //token invalid
                res.sendStatus(403);
            } else {
                //token valid
                // res.sendStatus(200);

                req.userId = tokenData.user[0].id;

                next();
            }
        });
    } else {
        //no token
        res.sendStatus(403)
    }
}

//UPDATED MYSQL VERSION
app.get('/:table/:id', (req, res, next) => {
  let table = req.params.table;
  let id = parseInt(req.params.id);
  app.db.query(`SELECT * FROM ${table} WHERE id = ${id}`, {} ,(err, results) => {

      if (err || results[0] == null){
          console.log(err);
          res.status(404).send('No matching results.');
      } else {
          res.json(results);
      }
  })
});

/**
 * Example protected route, frontend will need to pass authToken in the Authorization header.
 */

app.get('/protected', verifyToken, (req, res, next) => {
  jwt.verify(req.token, JWT_SECRET, (err, tokenData) => {
      if (err) {
          //token invalid
          res.sendStatus(403);
      } else {
          //token valid
          res.json(req.userId).sendStatus(200);
      }
  });
});

// USER
app.get('/user', verifyToken, (req, res, next) => {
    let users = []
    // get data from datasource
    app.db.query("SELECT id, username, role_id name FROM users WHERE id = " + req.userId, function (err, result) {
        if (err) throw err;
        let user = new User(result[0].username, result[0].id, result[0].role_id);
        // push to client
        res.json(user);
    });
})

/*
//used to check if there's a token, if so the token is extracted from the header and placed in req.token
function verifyToken(req, res, next) {
  let authHeader = req.headers['authorization'];
  if (typeof authHeader !== "undefined") {
      //token found in header
      req.token = authHeader.split(' ')[1];
      next();
  } else {
      //no token
      res.sendStatus(403)
  }
}
*/
