let express = require("express");
let router = module.exports = require('express').Router();
var Comment = require('../models/comment');
router.use(express.json());
// C:\Users\User\Desktop\Uni2\Project ServerClients\bloomingit\backend\controller\forumController.js
let QuestionController = require('../controller/questionController.js');
let ForumController = require('../controller/forumController.js');

router.post('/forum/question', (req, res) => {
    let question = req.body;

    question.date_asked = new Date();
    question.points = 0;

    let query = 'INSERT INTO `threads`(`assignment`, `chapter`, `content`, `date_asked`, ' +
        '`points`, `subject`, `title`, `user_id`) VALUES (?,?,?,?,?,?,?,?)';

    req.db.query(query, [question.assignment, question.chapter, question.content, question.date_asked.getFullYear()
    + '-' + (question.date_asked.getMonth() + 1) + '-' + question.date_asked.getDate(), question.points,
        question.subject, question.title, question.user_id], function (err, row) {
        if (err) throw err;

        row.forEach(element => {
            element.type = "question"
        });

        res.status(201).json(row);
    });

});

router.post('/forum/question/minimal/:title/:content/:subject/:user_id/:date_asked/:points', (req, res) => {
    let question = req.params;
    console.log(question)
    question.date_asked = new Date();
    question.points = 0;
    let query = 'INSERT INTO `threads`(`content`, `date_asked`, ' +
        '`points`, `subject`, `title`, `user_id`) VALUES (?,?,?,?,?,?)';

    req.db.query(query, [question.content, question.date_asked.getFullYear()
    + '-' + (question.date_asked.getMonth() + 1) + '-' + question.date_asked.getDate(), question.points,
        question.subject, question.title, question.user_id], function (err, row) {
        if (err) throw err;

        // row.forEach(element => {
        //     element.type = "question"
        // });

        res.status(201).json(row);
    });

});

router.get('/api/forum/questions', (req, res) => {
    let query = 'SELECT threads.id, content, date_asked, username, title ' +
        'FROM threads INNER JOIN users ON users.id = user_id';

    req.db.query(query, function (err, row) {
        if (err) throw err;
        res.status(200).json(row);
    })
})

router.post('/forum/:item/:id/vote', (req, res) => {
    let body = req.body;
    let user = body.userId;
    let id = req.params.id;
    let type = body.type;
    let item = req.params.item;
    let properties = [id, user];
    let VALUES = "(";

    if (item === 'question') {
        VALUES += 'null, null, ?, ?, ?)';
    } else if (item === 'user_assignment') {
        VALUES += 'null, ?, null, ?, ?)';
    } else if (item === 'comment') {
        VALUES += '?, null, null, ?, ?)';
    }
    if (type === 'up') {
        properties.push(true);
        req.db.query('SELECT * FROM user_vote WHERE user = ? && ' + item + ' = ?', [user, id], function (err, row) {
            if (row.length > 0) {

                req.db.query('UPDATE user_vote SET `type`=true WHERE user = ? && ' + item + ' = ?', [user, id], function (error, row2) {
                    if (error) throw error;
                    res.status(201).json({msg: 'updated'});
                })
            } else {
                req.db.query('INSERT INTO `user_vote` (`comment`, `user_assignment`, `question`, `user`, `type`) ' +
                    'VALUES ' + VALUES, properties, function (error, row2) {
                    if (error) throw error;
                    res.status(201).json({msg: 'created'});
                })
            }
        })
    } else if (type === 'down') {
        properties.push(false);

        req.db.query('SELECT * FROM user_vote WHERE user = ? && ' + item + ' = ?', [user, id], function (err, row) {
            if (row.length > 0) {
                req.db.query('UPDATE user_vote SET `type`=false WHERE user = ? && ' + item + ' = ?', [user, id], function (error, row2) {
                    if (error) throw error;
                    res.status(201).json({msg: 'updated'});
                })
            } else {
                req.db.query('INSERT INTO `user_vote` (`comment`, `user_assignment`, `question`, `user`, `type`) ' +
                    'VALUES ' + VALUES, properties, function (error, row2) {
                    if (error) throw error;
                    res.status(201).json({msg: 'created'});
                })
            }
        })
    }
});

router.get('/forum/:item/:id/votes', (req, res) => {
    let query = 'SELECT COUNT(*) AS votes FROM bloomingit.user_vote ' +
        'WHERE ' + req.params.item + '=? ' +
        'GROUP BY question';
    req.db.query(query, [req.params.id], function (err, row) {
        if (err) throw err;
        res.status(200).json(row[0].votes);
    });
})

router.get('/form/popular', (req, res) => {
    let query = 'SELECT threads.id, title, count(user_vote.id) as votes FROM threads ' +
        'INNER JOIN user_vote ' +
        'ON threads.id = user_vote.question ' +
        'WHERE user_vote.type=true ' +
        'GROUP BY threads.id ' +
        'ORDER BY votes';

    req.db.query(query, function (err, row) {
        if (err) throw err;

        res.status(200).json(row);
    });
});

router.get('/forum/:item/:id/comments', (req, rsp) => {
    let parentItem = "";
    let item = req.params.item;
    let id = req.params.id;
    let comments = [];
    if (item === 'userassignment') {
        parentItem = "user_assignment_id = " + id;
    } else if (item === 'question') {
        parentItem = "question_id = " + id;
    }

    let query = 'SELECT comments.id, parent_comment_id as parent, content, date_added, users.username FROM comments ' +
        'INNER JOIN users ON users.id = user_id ' +
        'WHERE ' + parentItem;
    req.db.query(query, [id], function (err, row) {
        if (err) throw err;
        let temp = row;
        let results = [];
        temp.forEach(element => {
            if (element.parent <= 0) {
                element.children = [];
                let comment = new Comment(element.id, element.parent, element.content, element.date_added, element.user_id, element.username, element.children)
                comments.push(comment)
                results.push(element);
                row.splice(row.indexOf(element), 1);
            }
        });

        temp = row;

        // second layer.
        temp.forEach(element => {// scond layer comment
            results.forEach(parent => {// loop through first layer comments
                if (element.parent === parent.id) { // if elements parent is euqal to the first layer comment's id.
                    element.children = []; // children for second comment will be an empty array.
                    let childComment = new Comment(element.id, element.parent, element.content, element.date_added, element.user_id, element.username, element.children)
                    parent.children.push(childComment) // add the second layer comment as a child of the first layer comment.
                    row.splice(row.indexOf(element), 1); // remove the second layer element from the results.
                }
            });
        });

        console.log(results);

        temp = row;

        temp.forEach(element => { // loop through the last layer comments
            results.forEach(parent => { // loop throu the first layer comments. 
                parent.children.forEach(child => { // loop through the second layer comment's children
                    if (element.parent === child.id) { // if elements parent equals the second layer comment's ID.
                        let childComment = new Comment(element.id, element.parent, element.content, element.date_added, element.user_id, element.username, null)
                        child.children.push(childComment)// add the last layer comment as a child of the second layer comment.
                        row.splice(row.indexOf(element), 1);
                    }
                });
            });
        })

        console.log(results);

        rsp.status(200).json(comments)
        //rsp.status(200).json(results);
    })
});

router.get('/api/forum/question/sort', (req, res) => {
    QuestionController.getSortedThreads(req, res);
});

router.get('/api/forum/question/:id', (req, rsp) => {
    let threadId = req.params.id;

    let query = 'SELECT threads.id, content, date_asked, username, title ' +
        'FROM threads INNER JOIN users ON users.id = user_id ' +
        'WHERE threads.id=?';

    req.db.query(query, threadId, function (err, row) {
        if (err) throw err;
        ForumController.getRatingWithPost(req, row, 'question', rsp);
    })
});

router.get('/api/forum/userassignment/:id', (req, rsp) => {
    let threadId = req.params.id;

    let query = 'SELECT user_assignments.id, content, date_asked, username, title FROM user_assignments INNER JOIN users ON users.id = user_id ' +
        'WHERE threads.id=?';

    req.db.query(query, threadId, function (err, row) {
        if (err) throw err;

        rsp.status(200).json(row);
    })
});

router.get('/api/forum/:item/:id/rating', (req, rsp) => {
    ForumController.getRating(req, rsp);
});

router.post('/forum/question/:threadId/comment', (req, res) => {
    let userId = req.body.user_id;
    req.body.date_added = new Date();
    req.db.query('Select username from users where id = ?', [userId], function (err, name) {
        if (err) throw err;
        if (name.length === 0) {
            res.status(404).json({error: "No such user!"});
        } else {
            let threadId = req.params.threadId;
            req.db.query('Select title from threads where id = ?', [threadId], function (err, title) {
                if (err) throw err;
                if (title.length === 0) {
                    res.status(404).json({error: "No such thread!"});
                } else {
                    let content = req.body.content;

                    let parentCommentId = req.body.parent_comment_id;
                    if (parentCommentId) {
                        req.db.query('Select date_added from comments where id = ?', [parentCommentId], function (err, date) {
                            if (err) throw err;
                            if (date.length === 0) {
                                res.status(404).json({error: "No such comment!"});
                            } else {

                                let query = 'Insert into comments (user_id, question_id, content, date_added, parent_comment_id, user_assignment_id) values (?, ?, ?, ?, ?, ?)';
                                req.db.query(query, [userId, threadId, content, new Date(), parentCommentId, req.body.user_assignment_id], function (err, row) {
                                    if (err) throw err;
                                    res.status(201).json(row);

                                })
                            }
                        })
                    } else {
                        let query = 'Insert into comments (user_id, parent_comment_id,user_assignment_id,rating,content,date_added,question_id) values (?, ?, ?, ?, ?,?,?)';
                        req.db.query(query, [userId, -1, -1, 0, content, "2020-2-29", threadId], function (err, row) {
                            if (err) throw err;
                            res.status(201).json(row);

                        })
                    }

                }
            })

        }
    });
});
