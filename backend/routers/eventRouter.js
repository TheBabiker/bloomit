let router = module.exports = require('express').Router();
var Event = require('../models/event');

// router.get('/events', (req, res, next) => {
//     let events = [];
//     // get data from datasource
//     router.db.query("SELECT events.name, events.time, events.date, events.location, files.name as thumbnailLocation, files.type as thumbnailType, event_category.name as category FROM events LEFT JOIN files ON events.thumbnail_id = files.id INNER JOIN event_category on events.category_id = event_category.id", function (err, result) {
//         if (err) throw err;
//         for (let i = 0; i < result.length; i++) {
//             // fill model
//             let eventDate = new Date(result[i].date);
//             let event = new Event(result[i].name, result[i].description, result[i].location, result[i].time,
//                 (eventDate.getDate() + "-" + (eventDate.getMonth() + 1) + "-" + eventDate.getFullYear()) ,
//                 null, result[i].category);

//             // some parsing to make everything ready
//             if(result[i].thumbnailLocation != null && result[i].thumbnailType === 'image') {
//                 event.setPicture(req.protocol + '://' + req.get('host') + "/" + result[i].thumbnailLocation);
//             }else if(result[i].thumbnailLocation != null && result[i].thumbnailType === 'url'){
//                 event.setPicture(result[i].thumbnailLocation);
//             }else {
//                 event.picture = null;
//             }
//             events.push(event);
//         }
//         // push to client
//         res.json(events);
//     });
// });



// router.get('/api/events', (req, res, next) =>{
//     let category;
//     if (req.query.category != null) {
//         switch (req.query.category) {
//             case 'charity': category = 'charity'; break;
//             case 'meetup': category = 'meetup'; break;
//             case 'parties': category = 'parties'; break;
//             case 'workshop': category = 'workshop'; break;
//             default: res.status(404).json('No matching results.');
//         }
//         req.db.collection('events').where('category', '==', category).get().then(snapshot => {
//             var arr = [];
//             if (snapshot.empty) {
//                 res.status(404).json('No matching results.');
//             } else {
//                 snapshot.forEach(doc => {
//                     arr.push(doc.data());
//                 });
//                 eventController.sortArrayById(arr)
//                 res.json(arr);
//             }
//         })
//             .catch(err => {
//                 res.status(501).send(err);
//             });
//     } else {
//         req.db.collection('events').get().then(snapshot => {
//             var arr = [];
//             if (snapshot.empty) {
//                 res.status(404).send('No matching results.');
//             }
//             snapshot.forEach(doc => {
//                 arr.push(doc.data());
//             });
//             eventController.sortArrayById(arr)
//             res.json(arr);
//         })
//             .catch(err => {
//                 res.status(501).send(err);
//             })
//     }
//   });

// router.get('/api/events/:id', (req, res, next) =>{
//     let id = parseInt(req.params.id)
  
//     req.db.collection('events').where('id', '==', id).get().then(snapshot => {
//         var arr = [];
//         if (snapshot.empty) {
//             res.status(404).send('No matching results.');
//         } else {
//             snapshot.forEach(doc => {
//             arr.push(doc.data());
//             });
//             res.json(arr);
//         }
//     })
//     .catch(err => {
//         res.status(501).send(err);
//     });
// });
  
// router.post('/api/events', (req, res, next) =>{
//     const newEvent = {
//         title: req.body.title,
//         description: req.body.description,
//         address: req.body.address,
//         date: req.body.date,
//         time: req.body.time,
//         attendees: [],
//         picture: req.body.picture,
//         category: req.body.category,
//         id: parseInt(req.body.id)
//     }
  
//     req.db.collection('events').where('id', '==', newEvent.id).get().then(snapshot => {
//         if (snapshot.empty) {
//             req.db.collection('events').add(newEvent)
//             res.status(201).json('done');
//         } else {
//             res.status(409).json('event already exist');
//         }
//     })
// });
  
// router.put('/api/events/:id', (req, res, next) => {
//     const editedEvent = {
//         title: req.body.title,
//         description: req.body.description,
//         address: req.body.address,
//         time: req.body.time,
//         date: req.body.date,
//         attendees: [],
//         picture: req.body.picture,
//         category: req.body.category,
//         id: parseInt(req.params.id)
//     }
    
//     req.db.collection('events').where('id', '==', editedEvent.id).get().then(snapshot => {
//         if (snapshot.empty) {
//             res.status(200).json('No matching results.')
//         } else {
//             let docId;
//             snapshot.forEach(doc => {
//                 docId = doc.id;
//             });
//             req.db.collection('events').doc(docId).set(editedEvent)
//             res.status(200).json('done')
//         }
//     })
// })
  
// router.delete('/api/events/:id', (req, res, next) => {
//     req.db.collection('events').where('id', '==', parseInt(req.params.id)).get().then(snapshot => {
//         if (snapshot.empty) {
//             res.status(200).json('No matching results.')
//         } else {
//             let docId;
//             snapshot.forEach(doc => {
//                 docId = doc.id;
//             });
//             req.db.collection('events').doc(docId).delete();
//             res.status(200).json('done');
//         }
//     })
// })