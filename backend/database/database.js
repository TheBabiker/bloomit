var mysql = require('mysql');

let config = process.env.NODE_ENV === 'dev' ?
    require('../config/development' ) :
    require('../config/production');

let db = mysql.createConnection(config);

module.exports = db;