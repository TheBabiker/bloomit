module.exports = class Event {

    constructor(id, name, description, location, time, date, pictures, category, attendees){
        this.id = id;
        this.name = name;
        this.description = description;
        this.location = location;
        this.time = time;
        this.date = date;
        this.pictures = pictures;
        this.category = category;
        this.attendees = attendees
    }

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
    }

    getDescription() {
        return this.description;
    }

    setDescription(description) {
        this.description = description;
    }

    getLocation() {
        return this.location;
    }

    setLocation(location) {
        this.location = location;
    }

    getTime() {
        return this.time;
    }

    setTime(time) {
        this.time = time;
    }

    getPictures() {
        return this.picture;
    }

    setPictures(picture) {
        this.picture.push(picture);
    }

    getCategory() {
        return this.category;
    }

    setCategory(category) {
        this.category = category;
    }

    getAttendees() {
        return this.attendees;
    }

    setAttendees(attendees) {
        this.attendees.push(attendees);
    }
}