
module.exports = class User {

    constructor(username, id, roleId){

        this.username = username;
        this.id = id;
        this.role_id = roleId;
        this.first_name = "";
        this.last_name = "";
    }

    toJson(){
        return JSON.parse(JSON.stringify(this));
    }
};