var lastId = 1;
module.exports = class FAQ {

    constructor(question, answer, subjectId){
        this.question = question;
        this.answer = answer;
        this.subjectId = subjectId;
    }

    getQuestion() {
        return this.question;
    }

    setQuestion(question) {
        this.question = question;
    }

    getAnswer() {
        return this.answer;
    }

    setAnswer(answer) {
        this.answer = answer;
    }

    getSubjectId() {
        return this.subjectId;
    }

    setSubjectdId(subjectId) {
        this.subjectId = subjectId;
    }

    
}