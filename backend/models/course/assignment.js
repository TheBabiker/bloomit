module.exports = class Assignment {
    constructor(id, name, chapterId, description, githubLink){
        this.id = id;
        this.name = name;
        this.chapterId = chapterId;
        this.description = description;
        this.githubLink = githubLink;
    }

    getId() {
        return this.id;
    }

    setId(id) {
        this.id = id;
    }

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
    }
    
    getChapterId() {
        return this.chapterId;
    }

    setChapterId(chapterId) {
        this.chapterId = chapterId;
    }

    getDescription() {
        return this.description;
    }

    setDescription(description) {
        this.description = description;
    }
    
    getGithubLink() {
        return this.githubLink;
    }

    setGithubLink(githubLink) {
        this.githubLink = githubLink;
    }
}