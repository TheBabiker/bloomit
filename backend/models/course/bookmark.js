var lastId = 1;
module.exports = class Bookmark {

    constructor(isBookmarked){
        this.isBookmarked = isBookmarked;
    }

    getIsBookmarked() {
        return this.isBookmarked;
    }

    setIsBookmarked(isBookmarked) {
        this.isBookmarked = isBookmarked;
    }
}