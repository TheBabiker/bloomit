var lastId = 1;
module.exports = class Chapter {

    constructor(id, name, courseId){
        this.id = id;
        this.name = name;
        this.courseId = courseId;
    }

    getId() {
        return this.id;
    }

    setId(id) {
        this.id = id;
    }

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
    }
    
    getCourseId() {
        return this.courseId;
    }

    setCourseId(courseId) {
        this.courseId = courseId;
    }
}