module.exports = class Video {
    constructor(id, title, link, chapterId){
        this.id = id;
        this.title = title;
        this.link = link;
        this.chapterId = chapterId;
    }

    getId() {
        return this.id;
    }

    setId(id) {
        this.id = id;
    }

    getTitle() {
        return this.title;
    }

    setTitle(title) {
        this.title = title;
    }
    
    getLink() {
        return this.link;
    }

    setLink(link) {
        this.link = link;
    }

    getChapterId() {
        return this.chapterId;
    }

    setChapterId(chapterId) {
        this.chapterId = chapterId;
    }
}