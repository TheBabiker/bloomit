var lastId = 1;
module.exports = class Course {

    constructor(id, name, groupId, overview, lecturerId){
        this.id = id
        this.name = name;
        this.groupId = groupId;
        this.overview = overview;
        this.lecturerId = lecturerId;
    }

    getId() {
        return this.id;
    }

    setId(id) {
        this.id = id;
    }

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
    }
    
    getGroupdId() {
        return this.groupId;
    }

    setGroupdId(groupId) {
        this.groupId = groupId;
    }

    getOverview() {
        return this.overview;
    }

    setOverview(Overview) {
        this.overview = overview;
    }

    getLecturerId() {
        return this.lecturerId;
    }

    setLecturerId(lecturerId) {
        this.lecturerId = lecturerId;
    }
}