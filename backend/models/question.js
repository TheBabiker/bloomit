const questions = [];
module.export = class Question {

    constructor(Assignment, Chapter, Content, DateAsked, Id, Rating, Subject, Title, User){
        this.Assignment = Assignment;
        this.Chapter = Chapter;
        this.Content = Content;
        this.DateAsked = DateAsked;
        this.Id = Id;
        this.Rating = Rating;
        this.Subject = Subject;
        this.Title = Title;
        this.User = User;
    }

    getAssignment() {
        return this.Assignment;
    }

    getChapter() {
        return this.Chapter;
    }

    getContent() {
        return this.Content;
    }

    getDateAsked() {
        return this.DateAsked;
    }

    getId() {
        return this.Id;
    }

    getRating() {
        return this.Rating;
    }

    getSubject() {
        return this.Subject;
    }

    getTitle() {
        return this.Title;
    }

    getUser() {
        return this.User;
    }
};