module.exports = class Comment {
    constructor(id, parentCommentId, content, dateAdded, userId, username, children) {
        this.id = id
        this.parentCommentId = parentCommentId
        this.content = content
        this.dateAdded = dateAdded
        this.username = username
        this.userId = userId
        this.children = children
    }
}
