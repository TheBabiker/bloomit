module.exports = {
  devServer: {
    disableHostCheck: true,
    'proxy': {
      '/api': {
        'target': 'https://samples.openweathermap.org',
        'pathRewrite': { '^/api': '' },
        'changeOrigin': true,
        'secure': false
      }
    }
  },
  pwa: {
    name: 'My App',
    themeColor: '#4DBA87',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',

    // configure the workbox plugin
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      swSrc: 'public/sw.js'
      // ...other Workbox options...
    }
  }
}
