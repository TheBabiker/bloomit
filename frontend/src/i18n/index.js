/**
 * Vue i18n
 *
 * @library
 *
 * http://kazupon.github.io/vue-i18n/en/
 */

// Lib imports
import Vue from 'vue'
import VueI18n from 'vue-i18n'

let messages = {
  en : {
    home : "Home",
    courses: "Courses",
    forum: "Forum",
    events: "Events"
  },
  nl : {
    home : "Startpagina",
    courses: "Cursussen",
    forum: "Forum",
    events: "Evenementen"
  }
}

Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'en',
  messages: messages
})

export default i18n
