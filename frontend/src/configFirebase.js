import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/messaging'

const config = {
  apiKey: 'AIzaSyBF1jQ5enzsqzfnBUdUACgJGnnnnrRuudA',
  authDomain: 'team5-be799.firebaseapp.com',
  databaseURL: 'https://team5-be799.firebaseio.com',
  projectId: 'team5-be799',
  storageBucket: 'team5-be799.appspot.com',
  messagingSenderId: '1044514964231',
  appId: '1:1044514964231:web:041673db0e1d978b99bba2',
  measurementId: 'G-1FKJWW5RXL'
}

firebase.initializeApp(config)

// Initialize Cloud Firestore through Firebase
let db = firebase.firestore()

db.enablePersistence({ experimentalTabSynchronization: true })

export default {
  db
}
