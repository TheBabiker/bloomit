/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */

export default[
  {
    path: '/',
    view: 'Homepage'
  },
  {
    path: '/signin',
    view: 'Loading'
  },
  {
    path: '/profile',
    view: 'ProfilePage'
  },
  {
    path: '/accounts',
    view: 'Accounts'
  },
  {
    path: '/reports',
    view: 'Reports'
  },
  {
    path: '/course/:id',
    view: 'Course',
    name: 'course'
  },
  {
    path: '/courses',
    view: 'Courses'
  },
  {
    path: '/course-creation',
    view: 'CourseCreation'
  },
  {
    path: '/course-edit/:id',
    view: 'CourseEdit',
    name: 'course-Edit'
  },
  {
    path: '/events',
    view: 'Events'
  },
  {
    path: '/threads',
    name: 'threads',
    view: 'Threads'
  },
  {
    path: '/add',
    name: 'question',
    view: 'QuestionForm'
  },
  {
    path: '/threads/:type/:id',
    name: 'forumpage',
    view: 'ForumPage'
  },
  {
    path: '/event/details', // TODO: make this dynamic link with the id of the event /event/:id
    view: 'EventDetail'
  },
  {
    path: '/edit',
    view: 'Event-editEvents.vue'
  }
]
