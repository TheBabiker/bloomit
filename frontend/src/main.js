// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas, faCommentAlt, faExclamationCircle, faSearch, faUser, faBars, faHome, faBookOpen, faUsers, faCalendarAlt, faGlobe, faSignOutAlt, faSort } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { sync } from 'vuex-router-sync'

import './components'
import './plugins'
import App from './App'
import i18n from '@/i18n'
import router from '@/router'
import store from '@/store'
import './registerServiceWorker'
import VueOffline from 'vue-offline'
import CKEditor from '@ckeditor/ckeditor5-vue'

library.add(fas)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(VueAxios, axios)
Vue.use(VueOffline)
library.add(faSearch, faUser, faBars, faHome, faBookOpen, faUsers, faCalendarAlt, faGlobe, faSignOutAlt)

library.add(faCommentAlt, faExclamationCircle, faSort)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

Vue.use(CKEditor);

Vue.use(VueOffline)

// Sync store with router
sync(store, router)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  i18n,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
